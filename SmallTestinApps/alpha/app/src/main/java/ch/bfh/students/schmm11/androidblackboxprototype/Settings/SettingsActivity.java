package ch.bfh.students.schmm11.androidblackboxprototype.Settings;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SettingsActivity extends AppCompatActivity{

    public static final String KEY_PREF_ACCELERATION = "pref_acceleration";
    public static final String KEY_PREF_GPS = "pref_gps";
    public static final String KEY_PREF_BAROMETER = "pref_barometer";
    public static final String KEY_PREF_GYRO = "pref_gyro";
    public static final String KEY_PREF_COMPASS = "pref_compass";
    public static final String KEY_PREF_VIDEO = "pref_video";
    public static final String KEY_PREF_AUDIO = "pref_audio";
    public static final String KEY_PREF_CAMERA = "pref_camera";
    public static final String KEY_PREF_RESOLUTION = "pref_resolution";


    public static final String KEY_PREF_LOOPRECORDING = "pref_loopRecording";
    public static final String KEY_PREF_LOOPLENGTH = "pref_loopLength";
    public static final String KEY_PREF_PARTLENGTH =  "pref_partLength";

    public static final String KEY_PREF_HOTSPOT = "pref_hotspot";
    public static final String KEY_PREF_HOTSPOTPASSWORD = "pref_hotspotPassword";

    public static final String KEY_PREF_CRASHRECOGNITION = "pref_crashRecognition";
    public static final String KEY_PREF_GFORCETHRESHOLD = "pref_gForceThreshold";
    public static final String KEY_PREF_AIRBAGTHRESHOLD = "pref_airbagThreshold";
    public static final String KEY_PREF_GYROSCOPETHRESHOLD = "pref_gyroscopeThreshold";

    public static final String KEY_PREF_AUTOSTART = "pref_autoStart";

    public static final String KEY_PREF_REALTIMETRANSFER = "pref_realtimeTransfer";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}