package ch.bfh.students.schmm11.androidblackboxprototype.RealTimeTransfer;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.UnsupportedEncodingException;

import ch.bfh.students.schmm11.androidblackboxprototype.MainActivity;

public class RealTimeSender {
    private String TAG = "RealtimeSender";
    private Context context;



    private BroadcastReceiver listener = new BroadcastReceiver() {

        @Override
        public void onReceive( Context context, Intent intent ) {
            String data = intent.getStringExtra("DATA");
            sendData(data);
        }

    };





    //Nearby
    private Strategy strategy = Strategy.P2P_POINT_TO_POINT;
    private String ownEndpointname, partnerEndpointName;
    private String serviceId = "BlackboxDataReceiver";
    private ConnectionsClient mConnectionsClient;
    private boolean isConnected = false;

    //Nearby Methods
    //Nearby ConnectionLifeCycle
    private final ConnectionLifecycleCallback mConnectionLifecycleCallback =
            new ConnectionLifecycleCallback() {
                @Override
                public void onConnectionInitiated(String endpointId, ConnectionInfo connectionInfo) {
                    // Automatically accept the connection on both sides.
                    partnerEndpointName = endpointId;
                    Log.d(TAG, "onConnectionInit from" + endpointId);
                    mConnectionsClient.acceptConnection(endpointId, mPayloadCallback);
                }

                @Override
                public void onConnectionResult(String endpointId, ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode()) {
                        case ConnectionsStatusCodes.STATUS_OK:
                            Log.d(TAG, "We're sucessfully Nearby connected!");
                            Toast.makeText(context,"Connected to a receiver", Toast.LENGTH_LONG).show();
                            startSendingData();
                            break;
                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            Log.d(TAG, "The connection was rejected by one or both sides");
                            break;
                        default:
                            Log.d(TAG, "The connection was broken before it was accepted.");
                            break;
                    }
                }
                @Override
                public void onDisconnected(String endpointId) {
                    isConnected = false;

                    Log.d(TAG, "We're disconnected");
                }
            };



    //receive a Payload
    private final PayloadCallback mPayloadCallback =
            new PayloadCallback() {
                @Override
                public void onPayloadReceived(String endpointId, Payload payload) {
                    // Nothing to Do since we are the sender
                }
                @Override
                public void onPayloadTransferUpdate(String endpointId, PayloadTransferUpdate update) {
                    if (update.getStatus() == PayloadTransferUpdate.Status.SUCCESS) {
                        // Nothing to Do since we are the sender
                    }
                }
            };


    public RealTimeSender(Context context){
        //Maybe pass the context another way...
        this.context = context;
        mConnectionsClient = Nearby.getConnectionsClient(context);

    }

    public void launchAdvertising() {
        //only advertise when no
        if(!isConnected){
            mConnectionsClient
                    .startAdvertising(
                            /* endpointName= */ "AdvertiserDevice",
                            /* serviceId= */ serviceId,
                            mConnectionLifecycleCallback,
                            new AdvertisingOptions(strategy))
                    .addOnSuccessListener(
                            new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void unusedResult) {
                                    Log.d(TAG,"Now Advertising....");
                                    Toast.makeText(context, "RealTimeTransfer:, now Advertising", Toast.LENGTH_LONG).show();
                                }
                            })
                    .addOnFailureListener(
                            new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d(TAG,"startAdvertising failed e: " +e);
                                }
                            });
        }
        else{
            Log.d(TAG, "Tried to launchAdvertsiing, but we are allreday connected");
        }
    }


    private void startSendingData() {
        isConnected = true;

    }
    public void stopTransfer() {
        isConnected = false;
        mConnectionsClient.stopAllEndpoints();
    }

    public void sendData(String str){
        try {
            mConnectionsClient.sendPayload(partnerEndpointName, Payload.fromBytes(str.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            Log.d(TAG, "We got an UnsupportedEncodingException: "+e);
            e.printStackTrace();
        }
    }
}
