package ch.bfh.students.schmm11.androidblackboxprototype.Saver;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;

public class CrashSaver implements SaverInterface {
    private static final String TAG = "Blackbox_CrashSaver";
    private File file;

    public CrashSaver() {
        if(isExternalStorageWritable()){
            Log.e("SUCESS", " Writable");
        }
        else{
            Log.e("Error", "Not Writable");
        }
        //file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "AccelerationTest.txt");
        //File filepath = new File(Environment.getExternalStorageDirectory().getPath() + "/Proj2Acceleration");
        File filepath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/AndroidBlackbox/Alarm");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HH-mm-ss");


        if (!filepath.exists()) {
            filepath.mkdirs();
        }
        file = new File( filepath.getPath(), sdf.format(System.currentTimeMillis())+"_Alarm.csv");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

    }
    public void save(String str){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file, true);
            OutputStreamWriter writer = new OutputStreamWriter(fileOutputStream);

            BufferedWriter fbw = new BufferedWriter(writer);

            fbw.append(str);
            //fbw.newLine();

            fbw.flush();
            fbw.close();

            fileOutputStream.flush();
            fileOutputStream.close();
            Log.d(TAG, "Saved Alarm Data");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
}

