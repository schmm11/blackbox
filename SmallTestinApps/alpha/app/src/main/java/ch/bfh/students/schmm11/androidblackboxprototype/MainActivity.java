package ch.bfh.students.schmm11.androidblackboxprototype;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import ch.bfh.students.schmm11.androidblackboxprototype.HotSpot.HotSpotReceiver;
import ch.bfh.students.schmm11.androidblackboxprototype.HotSpot.HotSpotSender;
import ch.bfh.students.schmm11.androidblackboxprototype.RealTimeTransfer.RealTimeSender;
import ch.bfh.students.schmm11.androidblackboxprototype.Sensor.AccelerationSensorListener;
import ch.bfh.students.schmm11.androidblackboxprototype.Sensor.BarometerSensorListener;
import ch.bfh.students.schmm11.androidblackboxprototype.Sensor.CompassSensorListener;
import ch.bfh.students.schmm11.androidblackboxprototype.Sensor.CrashDetectorListener;
import ch.bfh.students.schmm11.androidblackboxprototype.Sensor.GPSSensorListener;
import ch.bfh.students.schmm11.androidblackboxprototype.Sensor.GyroscopeSensorListener;
import ch.bfh.students.schmm11.androidblackboxprototype.Sensor.SensorInterface;
import ch.bfh.students.schmm11.androidblackboxprototype.Services.DeleteService;
import ch.bfh.students.schmm11.androidblackboxprototype.Services.RecorderService;
import ch.bfh.students.schmm11.androidblackboxprototype.Settings.SettingsActivity;
import ch.bfh.students.schmm11.androidblackboxprototype.Util.DataPurger;
import ch.bfh.students.schmm11.androidblackboxprototype.Util.RequestPermissions;


public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback{

    Handler sensorRecordRestartHandler = new Handler();
    private static boolean isRecording = false;

    //Buttons
    private ImageButton btn_Record, btn_Crash;
    private ImageView iv_StatusNearby, iv_StatusAcceleromter, iv_StatusLocation, iv_StatusBarometer, iv_StatusGyro, iv_StatusCompass, iv_StatusVideo, iv_StatusAudio;
    private SharedPreferences sharedPref;
    private ArrayList<SensorInterface> sensorList = new ArrayList<SensorInterface>();

    //camera
    public static SurfaceView mSurfaceView;
    public static SurfaceHolder mSurfaceHolder;
    public static boolean mPreviewrunning;

    //Alarm
    private boolean isInAlarmMode = false;
    private final int ALARM_THRESHOLD = 15000; // the Amount of time the User can react to the Alarm Dialog

    //Hotspot (Nearby)
    private HotSpotSender mHotSpotSender;

    //Real Time Transfer (Nearby)
    private RealTimeSender mRealTimeSender;
    private final String REALTIMETRANSFER_INTENT = "REALTIMETRANSFER_INTENT";
    private boolean isRealTimeTransferEnabled;

    // Debug Tag
    private static final String TAG = "Blackbox_MainActivity";




    // Broadcast receiver to alarm from the CrashDetectionListener
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //Received a Realtime Intent
            if (intent.getAction() == REALTIMETRANSFER_INTENT){
                Log.d(TAG, "Intent test");
                mRealTimeSender.sendData(intent.getStringExtra("data"));
            }
            //Received a Hotspot Intent
            else if(intent.getAction() == "ALARM_INTENT"){
                launchAlarm();
            }
            else{
                Log.wtf(TAG,"Received an impossible Intent Action: "+intent.getAction());
            }
        }




    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Let the Screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //print versions //Todo: build.gradle(Module:app) and change Version everytime you begin to work on!!
        setTitle("AndroidBlackBoxPrototype (Version: " + BuildConfig.VERSION_NAME + " BuildType: " + BuildConfig.BUILD_TYPE + ")");

        // Settings:
        // Set the default settings the first time the app is started
        PreferenceManager.setDefaultValues(this.getApplicationContext(), R.xml.preferences, false);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        this.isRealTimeTransferEnabled = sharedPref.getBoolean (SettingsActivity.KEY_PREF_REALTIMETRANSFER, false);


        //set landscape mode
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //Init Surface for Camera
        mSurfaceView = findViewById(R.id.surfaceView);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        RequestPermissions myPermReq = new RequestPermissions();
        //Todo: Ask permissions not only quick and dirty :)
        myPermReq.requestAllPermissions(this);

        //Init every Status Image
        this.iv_StatusNearby = findViewById(R.id.iv_StatusNearby);
        this.iv_StatusAcceleromter = findViewById(R.id.iv_StatusAccelerometer);
        this.iv_StatusLocation = findViewById(R.id.iv_StatusLocation);
        this.iv_StatusBarometer = findViewById(R.id.iv_StatusBarometer);
        this.iv_StatusGyro = findViewById(R.id.iv_StatusGyro);
        this.iv_StatusCompass = findViewById(R.id.iv_StatusCompass);
        this.iv_StatusVideo = findViewById(R.id.iv_StatusVideo);
        this.iv_StatusAudio = findViewById(R.id.iv_StatusAudio);

        //TODO Set on Click Listener for Nearby: Experimental
        iv_StatusNearby.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if(sharedPref.getBoolean (SettingsActivity.KEY_PREF_REALTIMETRANSFER, false)) {
                    mRealTimeSender.launchAdvertising();
                }
            }
        });

        // Record Button
        this.btn_Record = findViewById(R.id.btn_Record);
        btn_Record.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if(!isRecording){
                    //Start Recording
                    launchRecording();
                    isRecording = true;
                    btn_Record.setImageResource(R.drawable.icon_recording_stop);
                    Log.d(TAG, "Recording Button pressed => Start");
                }
                else if(isRecording){
                    //Stop Recording
                    stopRecording();
                    isRecording = false;
                    btn_Record.setImageResource(R.drawable.icon_recording_start);
                    Log.d(TAG, "Recording Button pressed => Stop");
                }
            }
        });

        // Crash Button
        this.btn_Crash = findViewById(R.id.btn_Crash);
        btn_Crash.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                //Todo: call the deleter to clear the delete queue
                if (isRecording) {
                    launchAlarm(); // Start an Alarm
                }
            }
        });

        //Change the Status Images
        changeStatusImages();

        //Init RealTimeTransfer
        if(isRealTimeTransferEnabled){
            //if(mRealTimeSender == null){
                mRealTimeSender = new RealTimeSender(getApplicationContext()); //Can be dumb to create context this way...
                Log.d(TAG,"Started Nearby RealtimeSender");
            //}
        }


    } // end onCreate()



    @Override
    public void onResume() {
        super.onResume();
        //we should do the coloring of the record status here, because onCreate is NOT called when back from settings fragment
        changeStatusImages();

        //Register local Broadcast
        IntentFilter filter = new IntentFilter(CrashDetectorListener.BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter);
        filter = new IntentFilter(REALTIMETRANSFER_INTENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter);


    }

    @Override
    protected void onPause() {
        super.onPause();
        // unregister local broadcast
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.btn_Settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.btn_Purge){
            //Alertdialog (Yes/No Box)
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            final  DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            DataPurger dataPurger = new DataPurger();
                            if(isRecording){stopRecording();}
                            if(dataPurger.purgeData()){
                                Toast.makeText(MainActivity.this, "Data purged!", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(MainActivity.this, "Error while purging! Data NOT purged!", Toast.LENGTH_LONG).show();
                            }
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            Toast.makeText(MainActivity.this, "Aborted purging! Data NOT purged!", Toast.LENGTH_LONG).show();;
                            break;
                    }
                }
            };

            builder.setTitle("Are you sure?")
                    .setMessage("This will delete ALL Data stored in the folder AndroidBlackbox!")
                    .setPositiveButton("Yes",dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }
        if (id == R.id.btn_HotspotReceiver) {
            Intent intent = new Intent(this, HotSpotReceiver.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.btn_HotspotSender) {
            //
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    *  Method which starts all the sensorlisteners
    */

    private void launchRecording() {
        if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_ACCELERATION, true)){
            AccelerationSensorListener accListener = new AccelerationSensorListener(this.getApplicationContext(), isRealTimeTransferEnabled);
            sensorList.add(accListener);
        }
        if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_GPS, true)){
            GPSSensorListener gpsListener = new GPSSensorListener(this.getApplicationContext(), isRealTimeTransferEnabled);
            sensorList.add(gpsListener);
        }
        if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_BAROMETER, true)){
            BarometerSensorListener barListener = new BarometerSensorListener(this.getApplicationContext(),isRealTimeTransferEnabled);
            sensorList.add(barListener);
        }
        if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_GYRO, true)){
            GyroscopeSensorListener gyroListener = new GyroscopeSensorListener(this.getApplicationContext(),isRealTimeTransferEnabled);
            sensorList.add(gyroListener);
        }
        if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_COMPASS, true)){
            CompassSensorListener compassListener = new CompassSensorListener(this.getApplicationContext(),isRealTimeTransferEnabled);
            sensorList.add(compassListener);
        }
        if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_CRASHRECOGNITION, true)) {
            CrashDetectorListener crashDetectorListener = new CrashDetectorListener(this.getApplicationContext());
            sensorList.add(crashDetectorListener);
        }

        //Services
        if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_VIDEO, true)) {
            Intent startRecorderServiceIntent = new Intent(MainActivity.this, RecorderService.class);
            startService(startRecorderServiceIntent);
        }

        if(sharedPref.getBoolean(SettingsActivity.KEY_PREF_LOOPRECORDING, true)) {
            Intent startDeleteServiceIntent = new Intent(MainActivity.this, DeleteService.class);
            startService(startDeleteServiceIntent);
        }

        // Start all the sensors
        for(SensorInterface sensor : sensorList){
            sensor.start();
        }

        // set handler to restart sensors for loop if enabled
        if(sharedPref.getBoolean(SettingsActivity.KEY_PREF_LOOPRECORDING,false)) {
            String intervalString = sharedPref.getString(SettingsActivity.KEY_PREF_PARTLENGTH, "60");
            int interval = Integer.valueOf(intervalString) * 1000;
            sensorRecordRestartHandler.postDelayed(restartSensorRecording, interval);
        }

        //Toast
        Toast.makeText(getBaseContext(), "Recording Started", Toast.LENGTH_SHORT).show();

    }






    /*
    * Method which stops all the SensorListeners
     */

    private void stopRecording(){
        // stop restarting recording if enabled
        if(sharedPref.getBoolean(SettingsActivity.KEY_PREF_LOOPRECORDING,false)) {
            try {
                sensorRecordRestartHandler.removeCallbacks(restartSensorRecording);
            }catch (Exception e){
                Log.e(TAG, e.getMessage());
            }
        }

        //stop camera recording
        Intent stopRecorderServiceIntent = new Intent(MainActivity.this, RecorderService.class);
        stopService(stopRecorderServiceIntent);
        //stop deleteService
        Intent stopDeleteServiceIntent = new Intent(MainActivity.this, DeleteService.class);
        stopService(stopDeleteServiceIntent);
        //stop all other sensors
        for(SensorInterface sensor : sensorList){
            sensor.stop();
        }
        //Clear the list
        sensorList.clear();

        // Toast
        Toast.makeText(getBaseContext(), "Recording Stopped", Toast.LENGTH_SHORT).show();
    }


    //Method for restarting Sensors
    private Runnable restartSensorRecording = new Runnable() {
        @Override
        public void run() {
            for(SensorInterface sensor : sensorList){
                sensor.restart();
            }
            String intervalString = sharedPref.getString(SettingsActivity.KEY_PREF_PARTLENGTH,"60");
            int interval = Integer.valueOf(intervalString)*1000;
            sensorRecordRestartHandler.postDelayed(restartSensorRecording,interval);
        }
    };



        private void changeStatusImages(){
            //Nearby

            //Acceleration
            if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_ACCELERATION, true)){
                iv_StatusAcceleromter.setImageResource(R.drawable.icon_acceleration_active);
            } else{
                iv_StatusAcceleromter.setImageResource(R.drawable.icon_acceleration);
            }
            //GPS
            if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_GPS, true)){
                iv_StatusLocation.setImageResource(R.drawable.icon_location_active);
            }else {
                iv_StatusLocation.setImageResource(R.drawable.icon_location);
            }
            //Barometer
            if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_BAROMETER, true)){
                iv_StatusBarometer.setImageResource(R.drawable.icon_barometer_active);
            }else {
                iv_StatusBarometer.setImageResource(R.drawable.icon_barometer);
            }
            //Gyro
            if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_GYRO, true)){
                iv_StatusGyro.setImageResource(R.drawable.icon_gyroscope_active);
            }else {
                iv_StatusGyro.setImageResource(R.drawable.icon_gyroscope);
            }
            //Compass
            if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_COMPASS, true)){
                iv_StatusCompass.setImageResource(R.drawable.icon_compass_active);
            }else {
                iv_StatusCompass.setImageResource(R.drawable.icon_compass);
            }
            //Video
            if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_VIDEO, true)){
                iv_StatusVideo.setImageResource(R.drawable.icon_video_active);
            }else {
                iv_StatusVideo.setImageResource(R.drawable.icon_video);
            }
            //Audio
            if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_AUDIO, true)){
                iv_StatusAudio.setImageResource(R.drawable.icon_audio_active);
            }else {
                iv_StatusAudio.setImageResource(R.drawable.icon_audio);
            }
            //Record Button
            if (isRecording){
                btn_Record.setImageResource(R.drawable.icon_recording_stop);
            }else{
                btn_Record.setImageResource(R.drawable.icon_recording_start);
            }
        }
    /*
    This Method launches an Alarm => Shows an Alert with the options to "Yes" (Save Data, launch HotSpot) and "No" (False-positive)
    If user doesn't react to the alert Dialog within 15 seconds, it will Save the Data and launch the hotspot.
     */
    private void launchAlarm() {
        Toast.makeText(this, "Alarm Test Alarm Test", Toast.LENGTH_LONG).show();
        if(isInAlarmMode){
            //Do Nothing since we are allready in alarm mode.
        }
        else{
            isInAlarmMode = true;
            //Play a Sound
            ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
            toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 1000);
            // Vibrate, for testing purpose
            Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(1000);

        //Create the alert dialog
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle("Alarm")
                    .setMessage("The Device measured an event. Shall we save the data?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //Restart the DeleterService
                            restartDeleteService();
                            isInAlarmMode = false;
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton("no", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // No => False Positive => Nothing happens
                            Toast.makeText(getApplicationContext(), "False Positive", Toast.LENGTH_LONG).show();
                            isInAlarmMode = false;
                            dialog.cancel();
                        }
                    })
                    .create();
            // Create a Listener with a Timer to know the passed Time
            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    final Button positiveButton = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                    final CharSequence positiveButtonText = positiveButton.getText();
                    new CountDownTimer(ALARM_THRESHOLD, 100) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            positiveButton.setText(String.format(Locale.getDefault(), "%s (%d)",
                                    positiveButtonText,
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) + 1));
                        }
                        @Override
                        public void onFinish() {
                            if(((AlertDialog) dialog).isShowing()){
                                //user did not reply to dialog Alert.
                                positiveButton.callOnClick();
                                dialog.cancel();
                                launchHotspot();
                                isInAlarmMode = false;

                            }
                        }
                    }.start();
                }
            });
            dialog.show();
        }}
    private void restartDeleteService() {
        if (sharedPref.getBoolean(SettingsActivity.KEY_PREF_LOOPRECORDING, true)) {
            //stop deleteService
            Intent stopDeleteServiceIntent = new Intent(MainActivity.this, DeleteService.class);
            stopService(stopDeleteServiceIntent);
            Intent startDeleteServiceIntent = new Intent(MainActivity.this, DeleteService.class);
            startService(startDeleteServiceIntent);
            int loopLength = Integer.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_LOOPLENGTH, "10"));
            Toast.makeText(MainActivity.this, "restarted DeleterService. the last " + loopLength + " minute(s) will not get deleted.", Toast.LENGTH_LONG).show();
        }
    }

    public void launchHotspot(){
        if(mHotSpotSender == null){
            mHotSpotSender = new HotSpotSender(getApplicationContext()); //Can be dumb to create context this way...
        }
        mHotSpotSender.activateHotspot();
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        //TODO: some nice shit

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        //TODO: some nice shit
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        //TODO: some nice shit
    }


}
