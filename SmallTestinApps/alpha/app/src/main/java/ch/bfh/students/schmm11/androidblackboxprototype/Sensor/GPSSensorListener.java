package ch.bfh.students.schmm11.androidblackboxprototype.Sensor;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import ch.bfh.students.schmm11.androidblackboxprototype.Saver.BarometerSaver;
import ch.bfh.students.schmm11.androidblackboxprototype.Saver.GPSSaver;

public class GPSSensorListener implements SensorInterface, LocationListener {
    private Sensor sensor;
    private GPSSaver saver = new GPSSaver();
    private static final String TAG = "Blackbox_GPSListener";
    LocationManager locationManager;
    private String strToSave = "";
    private double latitude;
    private double longitude;
    private double altitude;
    private float speed;
    private Boolean isRealTimeEnabled;
    private final String REALTIMETRANSFER_INTENT = "REALTIMETRANSFER_INTENT";
    private LocalBroadcastManager localBroadcastManager;


    @SuppressLint("MissingPermission")
    public GPSSensorListener(Context context, Boolean realTimeTransfer){
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        isRealTimeEnabled = realTimeTransfer;

        localBroadcastManager = LocalBroadcastManager.getInstance(context);
    }


    @SuppressLint("MissingPermission")
    @Override
    public void start() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,this);
        saver.save("Timestamp [ms]; Latitude [Degrees]; Longitude [Degrees]; Altitude (WGS 84)[m]; Speed [m/s]"+ System.getProperty ("line.separator")); //headlines
    }

    @Override
    public void stop() {
        locationManager.removeUpdates(this);
    }

    @Override
    public void restart() {
        saver = new GPSSaver();
        stop();
        start();
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "GPS onLocationChanged received");
        long timestamp = System.currentTimeMillis();


        // @TODO Get altitude, longitude, latitude and save it to strToSave --> Done
        latitude= location.getLatitude();
        longitude = location.getLongitude();
        altitude = location.getAltitude();
        speed = location.getSpeed();

        strToSave = timestamp  + ";" + latitude + ";"  + longitude + ";" + altitude + ";" + speed + System.getProperty ("line.separator");
        //@Todo do we need an amount of saving call like in AccelerationSensorListener? --> I think not because of refreshing time is 5 second or 10m (maybe when driving fast?)
        saver.save(strToSave);


                /*
        Send an Broadcast to the MainActivity
         */
        if (isRealTimeEnabled) {
            Intent localIntent = new Intent(REALTIMETRANSFER_INTENT);
            localIntent.putExtra("data", "gps;" + strToSave);
            localBroadcastManager.sendBroadcast(localIntent);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
