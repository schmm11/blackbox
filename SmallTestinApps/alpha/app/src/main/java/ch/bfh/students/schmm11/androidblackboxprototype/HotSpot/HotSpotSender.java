package ch.bfh.students.schmm11.androidblackboxprototype.HotSpot;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;

import ch.bfh.students.schmm11.androidblackboxprototype.Settings.SettingsActivity;
import ir.mahdi.mzip.zip.ZipArchive;


public class HotSpotSender {
    private String TAG = "EmergencySender";
    private Context context;

    //Needed for the password
    private SharedPreferences sharedPref;
    private String password;

    //File
    private String sourcePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)+"/AndroidBlackbox/";
    private String destinationPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)+"/";

    //Nearby
    private Strategy strategy = Strategy.P2P_POINT_TO_POINT;
    private String ownEndpointname, partnerEndpointName;
    private String serviceId = "ch.bfh.students.schmm11.emergency";
    private ConnectionsClient mConnectionsClient;

    //Nearby ConnectionLifeCycle
    private final ConnectionLifecycleCallback mConnectionLifecycleCallback =
            new ConnectionLifecycleCallback() {
                @Override
                public void onConnectionInitiated(String endpointId, ConnectionInfo connectionInfo) {
                    // Automatically accept the connection on both sides.
                    partnerEndpointName = endpointId;
                    Log.d(TAG, "onConnectionInit from" + endpointId);
                    mConnectionsClient.acceptConnection(endpointId, mPayloadCallback);
                }

                @Override
                public void onConnectionResult(String endpointId, ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode()) {
                        case ConnectionsStatusCodes.STATUS_OK:
                            Log.d(TAG, "We're sucessfully Nearby connected!");
                            Toast.makeText(context, "Nearby Hotspot: Connected to a receiver", Toast.LENGTH_LONG).show();
                            startSendingData();
                            break;
                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            Log.d(TAG, "The connection was rejected by one or both sides");
                            break;
                        default:
                            Log.d(TAG, "The connection was broken before it was accepted.");
                            break;
                    }
                }
                @Override
                public void onDisconnected(String endpointId) {
                    Log.d(TAG, "We're disconnected");
                }
            };

    //receive a Payload
    private final PayloadCallback mPayloadCallback =
            new PayloadCallback() {
                @Override
                public void onPayloadReceived(String endpointId, Payload payload) {
                    //Log.d(TAG, "Got a Payload from " + endpointId + ", but we should receive something....");
                }
                @Override
                public void onPayloadTransferUpdate(String endpointId, PayloadTransferUpdate update) {
                    if (update.getStatus() == PayloadTransferUpdate.Status.SUCCESS) {
                        Log.d(TAG, "PayloadTransferUpdate success => Finished");
                        Toast.makeText(context, "Nearby Hotspot: Finished transfer", Toast.LENGTH_LONG).show();

                        // Shall we stop every Connection after 1 sucessfull download?
                        deactivateHotSpot();
                    }
                }
            };


    public HotSpotSender(Context context){
        //Maybe pass the context another way...
        this.context = context;
        mConnectionsClient = Nearby.getConnectionsClient(context);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        password = sharedPref.getString(SettingsActivity.KEY_PREF_HOTSPOTPASSWORD,"12345");

        }

    /*
    This method is called from outside to start an hotspot
     */
    public void activateHotspot(){
        Log.d(TAG, "activateHotspot()");
        //First: Zip what we want
        //@TODO We should do this in an Background Thread
        prepareZip();
        //Second: Start Advertising
        startAdvertising();
    }
    /* Powered by MZIP
    See Dependencies under build.gradle for more Infos.
     */
    private void prepareZip() {
        Log.d(TAG, "preparezip();");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HH-mm-ss");

        String destination = destinationPath+ "AndroidBlackboxBackup_" + sdf.format(System.currentTimeMillis()) + ".zip";


        ZipArchive zipArchive = new ZipArchive();
        zipArchive.zip(sourcePath, destination,password); //Unfortunately, there is no way to get a onFinished Message
    }

    /*
    This method is called when an sucessfull Nearby connection is made
     */
    private void startSendingData(){
        mConnectionsClient.sendPayload(partnerEndpointName, getZipFile());
    }




    private void startAdvertising() {
        mConnectionsClient
                .startAdvertising(
                        /* endpointName= */ "AdvertiserDevice",
                        /* serviceId= */ serviceId,
                        mConnectionLifecycleCallback,
                        new AdvertisingOptions(strategy))
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unusedResult) {
                                Log.d(TAG,"Now Advertising....");
                                Toast.makeText(context, "Nearby Hotspot:, now Advertising", Toast.LENGTH_LONG).show();
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG,"startAdvertising failed e: " +e);
                            }
                        });
    }

    /*
    return the zip from the destination Path as Payload.
     */
    private Payload getZipFile() {
        File file = new File(destinationPath);
        Payload filePayload = null;
        try {
            filePayload = Payload.fromFile(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filePayload;
    }


    public void deactivateHotSpot() {
        mConnectionsClient.stopAllEndpoints();
    }

}

