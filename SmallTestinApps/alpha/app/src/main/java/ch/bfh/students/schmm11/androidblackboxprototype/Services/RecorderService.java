/*
 * Copyright (c) 2015, Picker Weng
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of CameraRecorder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project:
 *     CameraRecorder
 *
 * File:
 *     CameraRecorder.java
 *
 * Author:
 *     Picker Weng (pickerweng@gmail.com)
 */

package ch.bfh.students.schmm11.androidblackboxprototype.Services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import ch.bfh.students.schmm11.androidblackboxprototype.MainActivity;
import ch.bfh.students.schmm11.androidblackboxprototype.Settings.SettingsActivity;

public class RecorderService extends Service {
	private static final String TAG = "RecorderService";
	private SurfaceView mSurfaceView;
	private SurfaceHolder mSurfaceHolder;
	private static Camera mServiceCamera;
	private boolean mRecordingStatus;
	private MediaRecorder mMediaRecorder;
	private SharedPreferences sharedPref;
	private  Camera.Parameters cameraParameters;


	@Override
	public void onCreate() {
		mSurfaceView = MainActivity.mSurfaceView; //do we need this?
		mSurfaceHolder = MainActivity.mSurfaceHolder;

		mRecordingStatus = false;
		
		super.onCreate();
	}




	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);

		sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());

			startRecording();

		return START_STICKY;
	}


	@Override
	public void onDestroy() {

		stopRecording();
		mRecordingStatus = false;
		
		super.onDestroy();
	}

	private boolean startRecording(){
		try {
			if (prepareCamera()){
				if(setPreview()){
					mServiceCamera.unlock();
					mMediaRecorder = new MediaRecorder();
					if(configureMediaRecorder()){
						mMediaRecorder.prepare();
						mMediaRecorder.start();
						mRecordingStatus = true;
					}
				}
			}
			Toast.makeText(getBaseContext(), String.format("Selected Size (%d x %d)", cameraParameters.getPreviewSize().width, cameraParameters.getPreviewSize().height), Toast.LENGTH_SHORT).show();
			
			return true;

		} catch (IllegalStateException e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
			return false;

		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
			return false;
		}
	}



	//todo: describe method
	private boolean prepareCamera(){
		try {
			String camID = sharedPref.getString(SettingsActivity.KEY_PREF_CAMERA, "1");
			String resolution = sharedPref.getString(SettingsActivity.KEY_PREF_RESOLUTION, "720");
			String resolutionWidth = resolution.substring(0, resolution.indexOf("x"));                                        // stored in the Preference like "1920x1080"
			String resolutionHeight = resolution.substring(resolution.indexOf("x") + 1, resolution.length());

			mServiceCamera = Camera.open(Integer.valueOf(camID));

			cameraParameters = mServiceCamera.getParameters();

			final List<Size> listPreviewSize = cameraParameters.getSupportedPreviewSizes();
			for (Size size : listPreviewSize) {

				if (size.height == Integer.valueOf(resolutionHeight) & size.width == Integer.valueOf(resolutionWidth)) {
					cameraParameters.setPreviewSize(size.width, size.height);
				}
				Log.d(TAG, String.format("Supported Preview Size (%d, %d)", size.width, size.height));
			}
			cameraParameters.setRecordingHint(true);
			mServiceCamera.setParameters(cameraParameters);
		}catch (Exception e){
			Log.e(TAG,e.getMessage());
			return false;
		}
		return true;
	}

	//todo: describe method
	private boolean setPreview(){
		try {
			mServiceCamera.setPreviewDisplay(mSurfaceHolder);
			mServiceCamera.startPreview();
		}
		catch (IOException e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	//todo: describe method
	private boolean configureMediaRecorder(){
		try{
		mMediaRecorder.setCamera(mServiceCamera);
		//if audio is enabled
		if(sharedPref.getBoolean(SettingsActivity.KEY_PREF_AUDIO,true)){
			mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
		}
		mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
		mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
		//if audio is enabled
		if(sharedPref.getBoolean(SettingsActivity.KEY_PREF_AUDIO,true)){
			mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
		}
		mMediaRecorder.setVideoEncodingBitRate(10000000);
		mMediaRecorder.setVideoSize(cameraParameters.getPreviewSize().width, cameraParameters.getPreviewSize().height);
		mMediaRecorder.setOutputFile(getVideoFilePath().getPath());
		mMediaRecorder.setPreviewDisplay(mSurfaceHolder.getSurface());

		if(sharedPref.getBoolean(SettingsActivity.KEY_PREF_LOOPRECORDING,false)){
			int partLengthSeconds = Integer.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_PARTLENGTH,"30"));
			mMediaRecorder.setMaxDuration(partLengthSeconds*1000);
			mMediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
				@Override
				public void onInfo(MediaRecorder mr, int what, int extra) {
					if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
						restartVideoRecording();
					}
				}
			});
		}

		}catch (Exception e){
			Log.e(TAG,e.getMessage());
			return false;
		}
		return true;
	}


	//todo: describe method
	private void restartVideoRecording() {
		mMediaRecorder.stop();
		mMediaRecorder.reset();

		if (setPreview()) {
		    mServiceCamera.unlock();
			mMediaRecorder = new MediaRecorder();
			if (configureMediaRecorder())
			{
				try {
					mMediaRecorder.prepare();
					mMediaRecorder.start();
				} catch (Exception e) {
					Log.e(TAG, e.getMessage());
				}
			}
		}
	}

	//todo: describe method
	public void stopRecording() {
		try {
			mServiceCamera.reconnect();

		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			mMediaRecorder.stop();
			mMediaRecorder.reset();

			mServiceCamera.stopPreview();
			mMediaRecorder.release();

			mServiceCamera.release();
			mServiceCamera = null;
		}catch (Exception e){
			Log.e(TAG,e.getMessage());
		}
	}



	private File getVideoFilePath() {

        final File filepath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/AndroidBlackbox");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HH-mm-ss");


        if (!filepath.exists()) {
            filepath.mkdirs();
        }
        File file = new File(filepath.getPath(), sdf.format(System.currentTimeMillis()) + "_Video.mp4");

        return file;
    }
}
