package ch.bfh.students.schmm11.androidblackboxprototype.Sensor;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import ch.bfh.students.schmm11.androidblackboxprototype.Saver.CompassSaver;

public class CompassSensorListener implements SensorEventListener, SensorInterface {
    private Sensor sensor;
    private CompassSaver saver = new CompassSaver();
    private static final String TAG = "Blackbox_Compass Listener";
    private SensorManager mSensorManager;
    private String strToSave = "";
    private long amount;
    private boolean isRealTimeEnabled;
    private final String REALTIMETRANSFER_INTENT = "REALTIMETRANSFER_INTENT";
    private LocalBroadcastManager localBroadcastManager;


    public CompassSensorListener(Context context, Boolean realTimeTransfer){
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        localBroadcastManager = LocalBroadcastManager.getInstance(context);
        isRealTimeEnabled = realTimeTransfer;
    }



    @Override
    public void onSensorChanged(SensorEvent event) {
        Log.d(TAG, "Compass SensorData received");
        long timestamp = System.currentTimeMillis();
        //All values are in micro-Tesla (uT) and measure the ambient magnetic field in the X, Y and Z axis.
        this.strToSave += timestamp + ";" + event.timestamp + ";" + Float.toString(event.values[0]) +";" + Float.toString(event.values[1])+";"+ Float.toString(event.values[2]) + System.getProperty ("line.separator");
        amount++;

        /*
        Only call save() every n time => sonst verhacken sich die Save Befehle
         */
        if(amount % 6 == 0) {
            saver.save(strToSave);
            strToSave ="";
            Log.d(TAG, "onSensorChangedisCalled amount: " + amount);
        }

                /*
        Send an Broadcast to the MainActivity
         */
        if (isRealTimeEnabled) {
            Intent localIntent = new Intent(REALTIMETRANSFER_INTENT);
            localIntent.putExtra("data", "com;" + strToSave);
            localBroadcastManager.sendBroadcast(localIntent);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void start() {
        mSensorManager.registerListener((SensorEventListener) this, sensor, SensorManager.SENSOR_DELAY_GAME);
        saver.save("Timestamp [ms]; Event-Timestamp [ns]; X [uT]; Y [uT]; Z [uT]" + System.getProperty ("line.separator")); //headlines
    }

    @Override
    public void stop() {
        mSensorManager.unregisterListener(this, sensor);
    }

    @Override
    public void restart() {
        saver = new CompassSaver();
        stop();
        start();
    }
}
