package ch.bfh.ti.bachelor.foregroundservicetest;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SurfaceHolder.Callback {
    //camera
    public static SurfaceView mSurfaceView;
    public static SurfaceHolder mSurfaceHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button startButton = (Button) findViewById(R.id.btn_start);
        Button stopButton = (Button) findViewById(R.id.btn_stop);


        mSurfaceView = findViewById(R.id.surfaceView);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        startButton.setOnClickListener(this);
        stopButton.setOnClickListener(this);
    }

    public void onClick(View v){
        switch (v.getId()){
            case R.id.btn_start:
                Intent startIntent = new Intent(MainActivity.this, RecordingService.class);
                startIntent.setAction("ch.bfh.ti.bachelor.foregroundservicetest.action.startforeground");
                //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                //    startForegroundService(startIntent);
                //}else{
                    startService(startIntent);
                //}
                break;
            case R.id.btn_stop:
                Intent stopIntent = new Intent(MainActivity.this, RecordingService.class);
                stopIntent.setAction("ch.bfh.ti.bachelor.foregroundservicetest.action.stopforeground");
                //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                //    startForegroundService(stopIntent);
                //}else{
                    startService(stopIntent);
                //}
                break;

            default:
                break;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

        Intent previewChangedIntent = new Intent(MainActivity.this, RecordingService.class);
        previewChangedIntent.setAction("ch.bfh.ti.bachelor.foregroundservicetest.action.renewpreview");

        startService(previewChangedIntent);


    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }
}
