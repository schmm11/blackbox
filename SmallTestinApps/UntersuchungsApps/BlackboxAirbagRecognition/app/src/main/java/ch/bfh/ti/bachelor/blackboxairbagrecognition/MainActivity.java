package ch.bfh.ti.bachelor.blackboxairbagrecognition;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;


public class MainActivity extends AppCompatActivity implements SensorEventListener {

    /* Enter a good AlertValue in the following line:*/
    static final double alertValue = 0.2F;


    private TextView txt_ActValue;
    private TextView txt_AirbagRecogn;
    private SensorManager mSensorManager;
    private double actualPressure;
    private double previousPressure;
    private double differencePressure;

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd - HH:mm:ss");
    private static final NumberFormat nf = new DecimalFormat(".00");



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_ActValue = (findViewById(R.id.txt_ActValue));
        txt_AirbagRecogn = (findViewById(R.id.txt_AirbagRecogn));

        mSensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener((SensorEventListener) this, mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE), SensorManager.SENSOR_DELAY_GAME);


    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        actualPressure = sensorEvent.values[0];
        txt_ActValue.setText("Actual pressure: " + nf.format(actualPressure) + " mBar");

        if (previousPressure != 0) {
            differencePressure = actualPressure - previousPressure;
        } else {
            differencePressure = 0;
        }


        if (differencePressure < 0) {
            differencePressure = differencePressure * (-1);
        }

        if (differencePressure > alertValue) {

            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String time = sdf.format(timestamp);

            txt_AirbagRecogn.setText(time + " - " + nf.format(differencePressure) + " mBar\n" + txt_AirbagRecogn.getText());
        }

        previousPressure = actualPressure;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

}
