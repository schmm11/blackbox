package ch.bfh.students.schmm11.v2nearbyreceiver;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo;
import com.google.android.gms.nearby.connection.DiscoveryOptions;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

public class HotspotReceiver extends AppCompatActivity {

    private ProgressBar progressBar;
    private TextView textView;
    private String TAG = "HotspotReceiver";

    //Nearby
    private Strategy strategy = Strategy.P2P_POINT_TO_POINT;
    private String ownEndpointname, partnerEndpointName;
    private String serviceId = "ch.bfh.students.schmm11.emergency";
    private ConnectionsClient mConnectionsClient;
    private int amountPayloads = 0; //Just for testing purpoe, can be deleted

    //Nearby Connection Lifecycle
    private final ConnectionLifecycleCallback mConnectionLifecycleCallback =
            new ConnectionLifecycleCallback() {
                @Override
                public void onConnectionInitiated(String endpointId, ConnectionInfo connectionInfo) {
                    // Automatically accept the connection on both sides.
                    partnerEndpointName = endpointId;
                    Log.d(TAG, "onConnectionInit from" + endpointId);
                    mConnectionsClient.acceptConnection(endpointId, mPayloadCallback);
                }

                @Override
                public void onConnectionResult(String endpointId, ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode()) {
                        case ConnectionsStatusCodes.STATUS_OK:
                            Log.d(TAG, "We're sucessfully Nearby connected!");
                            textView.append(" Endpoint found and connected "+System.getProperty("line.separator"));

                            //send Payload
                            //sendData(getZipFile());
                            break;
                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            Log.d(TAG, "The connection was rejected by one or both sides");
                            break;
                        default:
                            Log.d(TAG, "The connection was broken before it was accepted.");
                            break;
                    }
                }
                @Override
                public void onDisconnected(String endpointId) {
                    //mIsConnected = false;
                    Log.d(TAG, "We're disconnected");
                   textView.append("Disconnected from Endpoint"+ System.getProperty("line.separator"));
                }
            };
    //Nearby Payload Callback
    private final PayloadCallback mPayloadCallback =
            new PayloadCallback() {
                @Override
                public void onPayloadReceived(String endpointId, Payload payload) {
                    Log.d(TAG, "Got a Junk Payload from " + endpointId);
                    amountPayloads++;

                }
                @Override
                public void onPayloadTransferUpdate(String endpointId, PayloadTransferUpdate update) {
                    if (update.getStatus() == PayloadTransferUpdate.Status.SUCCESS) {
                        textView.append("Transferupdate has Status Sucess (Amound of Payloads: "+ amountPayloads +"=> Finish");
                        Log.d(TAG, "payload transmitted");

                        //Do something with the Payload HERE


                    }
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotspot_receiver);

        textView = findViewById(R.id.txt_Status);
        progressBar = findViewById(R.id.progressBar);
        mConnectionsClient = Nearby.getConnectionsClient(this);

        textView.setText("Activity started."+ System.getProperty("line.separator") +" Searching for Hotspots with the ServiceTag "+serviceId+ " now..." + System.getProperty("line.separator"));
        startDiscovering();
    }


    private void startDiscovering() {
        textView.append(" Starting Discovery... "+System.getProperty("line.separator"));

        mConnectionsClient
                .startDiscovery(
                        serviceId,
                        new EndpointDiscoveryCallback() {
                            @Override
                            public void onEndpointFound(String endpointId, DiscoveredEndpointInfo info) {
                                Log.d(TAG, "Endpoint Found, Name is: "+ endpointId);
                                if (serviceId.equals(info.getServiceId())) {
                                    partnerEndpointName = endpointId;

                                    // Ask for a Connection
                                    mConnectionsClient
                                            .requestConnection("DiscovererName", partnerEndpointName, mConnectionLifecycleCallback)
                                            .addOnFailureListener(
                                                    new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            Log.d(TAG, "onEndpointDiscovered failed");
                                                        }
                                                    });
                                }
                            }
                            @Override
                            public void onEndpointLost(String endpointId) {
                                Log.d(TAG, "onEndpointLost failed");
                            }
                        },
                        new DiscoveryOptions(strategy))
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unusedResult) {
                                textView.append(" Endpoint found "+System.getProperty("line.separator"));

                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "Discovery failed");
                            }
                        });
    }



}
