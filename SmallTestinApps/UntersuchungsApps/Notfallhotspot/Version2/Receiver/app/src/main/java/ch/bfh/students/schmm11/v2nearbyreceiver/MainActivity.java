package ch.bfh.students.schmm11.v2nearbyreceiver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button startHotspotReceiver;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startHotspotReceiver = findViewById(R.id.btn_Hotspotreceiver);
        startHotspotReceiver.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                startHotspotActivity();
            }
        });
        

        

    }

    private void startHotspotActivity() {
        Intent intent = new Intent(this, HotspotReceiver.class);
        startActivity(intent);

    }
}
