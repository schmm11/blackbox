package ch.bfh.students.schmm11.unfallerkennungv01;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private TextView txt1;
    private TextView txt2;
    private double gForce = 0;
    private double lastGForce = 0;
    private int amount = 0;

    private SensorManager mSensorManager;
    private float[] mAccelGravityData = new float[3];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt1 = (TextView) findViewById(R.id.txt1);
        txt2 = (TextView) findViewById(R.id.txt2);
        txt2.setText(" ");
        txt1.setText(" ");
        mSensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener((SensorEventListener) this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //txt2.append(event.values.toString() + "/n");

/* Is this right?
        gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
        gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
        gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

        linear_acceleration[0] = event.values[0] - gravity[0];
        linear_acceleration[1] = event.values[1] - gravity[1];
        linear_acceleration[2] = event.values[2] - gravity[2];
*/
        txt2.append(event.values.toString() + "/n");
        mAccelGravityData[0]=(mAccelGravityData[0]*2+event.values[0])*0.33334f;
        mAccelGravityData[1]=(mAccelGravityData[1]*2+event.values[1])*0.33334f;
        mAccelGravityData[2]=(mAccelGravityData[2]*2+event.values[2])*0.33334f;

        // Calculate directionless GForce: (x^2 + y^2 + z^2 )/9.8
        gForce = (mAccelGravityData[0]*mAccelGravityData[0] + mAccelGravityData[1]*mAccelGravityData[1] + mAccelGravityData[2]*mAccelGravityData[2] )/9.81;

        txt2.setText(String.format("%.2f", gForce));

        double deltaGForce = gForce- lastGForce;

        //When delta is bigger than 4 => Alarm
        if( deltaGForce > 4.00 || deltaGForce < -4.00 ){
            txt1.setText("ALAAAARM: DeltaGForce ist: " + String.format("%.2f", gForce) + "Gesamtanzahl Crashes: " + amount);
            amount++;
        }

        lastGForce = gForce;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
