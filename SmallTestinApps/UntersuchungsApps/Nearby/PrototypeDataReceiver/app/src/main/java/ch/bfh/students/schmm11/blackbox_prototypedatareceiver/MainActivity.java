package ch.bfh.students.schmm11.blackbox_prototypedatareceiver;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo;
import com.google.android.gms.nearby.connection.DiscoveryOptions;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.UnsupportedEncodingException;

import ch.bfh.students.schmm11.blackbox_prototypedatareceiver.Saver.AccelerationSaver;
import ch.bfh.students.schmm11.blackbox_prototypedatareceiver.Saver.BarometerSaver;
import ch.bfh.students.schmm11.blackbox_prototypedatareceiver.Saver.CompassSaver;
import ch.bfh.students.schmm11.blackbox_prototypedatareceiver.Saver.GPSSaver;
import ch.bfh.students.schmm11.blackbox_prototypedatareceiver.Saver.GyroscopeSaver;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private TextView textAmount;
    private String TAG = "BlackboxDataReceiver";

    //SaversRT
    private AccelerationSaver accelerationSaver;
    private BarometerSaver barometerSaver;
    private GyroscopeSaver gyroscopeSaver;
    private GPSSaver gpsSaver;
    private CompassSaver compassSaver;


    //Nearby
    private Strategy strategy = Strategy.P2P_POINT_TO_POINT;
    private String partnerEndpointName;
    private String serviceId = "myUniqueID";
    private ConnectionsClient mConnectionsClient;
    private int amountPayloads = 0; //Just for testing purpoe, can be deleted
    private long amountReceived;







    //NEarby Methods
    //Nearby Connection Lifecycle
    private final ConnectionLifecycleCallback mConnectionLifecycleCallback =
            new ConnectionLifecycleCallback() {
                @Override
                public void onConnectionInitiated(String endpointId, ConnectionInfo connectionInfo) {
                    // Automatically accept the connection
                    partnerEndpointName = endpointId;
                    Log.d(TAG, "onConnectionInit from" + endpointId);
                    mConnectionsClient.acceptConnection(endpointId, mPayloadCallback);
                }

                @Override
                public void onConnectionResult(String endpointId, ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode()) {
                        case ConnectionsStatusCodes.STATUS_OK:
                            //Finished establishing a correct connection
                            Log.d(TAG, "We're sucessfully Nearby connected!");
                            textView.append(" Endpoint found and connected "+System.getProperty("line.separator"));

                            break;
                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            Log.d(TAG, "The connection was rejected by one or both sides");
                            break;
                        default:
                            Log.d(TAG, "The connection was broken before it was accepted.");
                            break;
                    }
                }
                @Override
                public void onDisconnected(String endpointId) {
                    //mIsConnected = false;
                    Log.d(TAG, "We're disconnected");
                    textView.append("Disconnected from Endpoint"+ System.getProperty("line.separator"));
                }
            };
    //Nearby Payload Callback
    private final PayloadCallback mPayloadCallback =
            new PayloadCallback() {
                @Override
                public void onPayloadReceived(String endpointId, Payload payload) {
                    Log.d(TAG, "Got a Payload from, total amount: " + amountReceived);
                    amountReceived++;
                    refreshAmountTextView();
                    savePayload(payload);

                }
                @Override
                public void onPayloadTransferUpdate(String endpointId, PayloadTransferUpdate update) {
                    if (update.getStatus() == PayloadTransferUpdate.Status.SUCCESS) {
                        //Nothing do to here
                    }
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.txt_Status);
        textAmount = findViewById(R.id.txt_Amount);
        this.amountReceived= 0;

        //init Saver
        accelerationSaver = new AccelerationSaver();
        barometerSaver = new BarometerSaver();
        gyroscopeSaver = new GyroscopeSaver();
        compassSaver = new CompassSaver();
        gpsSaver = new GPSSaver();

        mConnectionsClient = Nearby.getConnectionsClient(this);
        textView.setText("Activity started."+ System.getProperty("line.separator") +" Searching for Hotspots with the ServiceTag "+serviceId+ " now..." + System.getProperty("line.separator"));
        startDiscovering();
    }

    /*
    This Method takes a payload, analyzes it and give it to the corresponing Saver
     */
    private void savePayload(Payload payload) {
        String payloadString = "";
        try {
            payloadString = new String(payload.asBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // The 4 first letters in the PayloadString defines the type (example acc; or gyr;
        String typeString = payloadString.substring(0,4);
        String saveString = payloadString.substring(4);

        switch (typeString){
            case "acc;":
                accelerationSaver.save(saveString);
                break;
            case "bar;":
                barometerSaver.save(saveString);
                break;
            case "gyr;":
                gyroscopeSaver.save(saveString);
                break;
            case "com;":
                compassSaver.save(saveString);
                break;
            case "gps;":
                gpsSaver.save(saveString);
                break;
            default:
                Log.wtf(TAG, "Unknow type... HOW can this happen?");

            }

        }

    private void startDiscovering() {
        textView.append(" Starting Discovery... "+System.getProperty("line.separator"));

        mConnectionsClient
                .startDiscovery(
                        serviceId,
                        new EndpointDiscoveryCallback() {
                            @Override
                            public void onEndpointFound(String endpointId, DiscoveredEndpointInfo info) {
                                Log.d(TAG, "Endpoint Found, Name is: "+ endpointId);
                                if (serviceId.equals(info.getServiceId())) {
                                    partnerEndpointName = endpointId;

                                    // Ask for a Connection
                                    mConnectionsClient
                                            .requestConnection("DiscovererName", partnerEndpointName, mConnectionLifecycleCallback)
                                            .addOnFailureListener(
                                                    new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            Log.d(TAG, "onEndpointDiscovered failed");
                                                        }
                                                    });
                                }
                            }
                            @Override
                            public void onEndpointLost(String endpointId) {
                                Log.d(TAG, "onEndpointLost failed");
                            }
                        },
                        new DiscoveryOptions(strategy))
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unusedResult) {
                                //textView.append(" Sucessfull Discovery "+System.getProperty("line.separator"));

                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "Discovery failed");
                            }
                        });
    }
    public void refreshAmountTextView(){
        this.textAmount.setText("Amount Received: " + amountReceived);
    }
    }
