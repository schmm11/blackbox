package ch.bfh.ti.bachelor.blackboxallsensorservicesprototype;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {
    Switch switchAcceleration;
    Switch switchBarometer;
    Switch switchCrashRecognition;
    Switch switchGPS;
    Switch switchGyro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Switch switchAcceleration = (Switch) findViewById(R.id.switch_Barometer);
        Switch switchBarometer = (Switch) findViewById(R.id.switch_Barometer);
        Switch switchCrashRecognition = (Switch) findViewById(R.id.switch_Barometer);
        Switch switchGPS = (Switch) findViewById(R.id.switch_Barometer);
        Switch switchGyro = (Switch) findViewById(R.id.switch_Barometer);

        switchBarometer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Intent i=new Intent(MainActivity.this, BarometerService.class);
                if (isChecked) {
                    //startService
                    startService(i);
                }else{
                    //stopService
                    stopService(i);
                }
            }
        });

        //checkServiceStates();
        switchAcceleration.setChecked(isMyServiceRunning(AccelerationService.class));
        switchBarometer.setChecked(isMyServiceRunning(BarometerService.class));
        switchCrashRecognition.setChecked(isMyServiceRunning(CrashRecognitionService.class));
        switchGPS.setChecked(isMyServiceRunning(GPSService.class   ));
        switchGyro.setChecked(isMyServiceRunning(GyroService.class));

    }

    protected void onResume() {
        super.onResume();
        //Todo: Check wich Services are running and set toggle Button to actual state
        //checkServiceStates(); close immediatly if running here
    }

    private void checkServiceStates(){
        switchAcceleration.setChecked(isMyServiceRunning(AccelerationService.class));
        switchBarometer.setChecked(isMyServiceRunning(BarometerService.class));
        switchCrashRecognition.setChecked(isMyServiceRunning(CrashRecognitionService.class));
        switchGPS.setChecked(isMyServiceRunning(GPSService.class   ));
        switchGyro.setChecked(isMyServiceRunning(GyroService.class));
    }


    //https://stackoverflow.com/questions/600207/how-to-check-if-a-service-is-running-on-android peter mortenson
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
