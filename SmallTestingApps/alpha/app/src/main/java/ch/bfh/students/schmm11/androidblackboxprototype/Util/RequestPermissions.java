package ch.bfh.students.schmm11.androidblackboxprototype.Util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import ch.bfh.students.schmm11.androidblackboxprototype.Settings.SettingsActivity;

public class RequestPermissions implements ActivityCompat.OnRequestPermissionsResultCallback {

    //Permission request constants
    final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 100;
    final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 101;
    final int  MY_PERMISSIONS_REQUEST_VIBRATE = 102;
    final int  MY_PERMISSIONS_REQUEST_RECORD_VIDEO = 103;
    final int  MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 104;
    final int  MY_PERMISSIONS_REQUEST_CAMERA = 105;


    // short version quick and dirty
    public void requestAllPermissions(Activity thisActivity){
        String[] permissions = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.VIBRATE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
        };
        requestPermission(thisActivity, permissions,100);
    }



    //see here: https://developer.android.com/training/permissions/requesting#java
    public boolean requestAllNeededPermissions (Activity thisActivity, Context context){

        //get all activated recordings
        List<String> permissions;
        permissions = getAllNeededPermissions(context);

        //check each permission
        for(String permission: permissions){
            if(checkPermission(thisActivity,permission,100)){
                //ok
            }else if(checkPermissionNeedsExplanation(thisActivity,permission,100)){
                //needs permission!!

            }else {
                requestPermission(thisActivity,new String[]{permission},100);
                //then we will get a callback!
            }
        }

        return true;
    }


    private boolean checkPermission(Activity thisActivity, String myPermission, int myPermissionRequestCode){
        if(ContextCompat.checkSelfPermission(thisActivity, myPermission)==PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }

    private boolean checkPermissionNeedsExplanation(Activity thisActivity, String myPermission, int myPermissionRequestCode){
        if (ActivityCompat.shouldShowRequestPermissionRationale(thisActivity,myPermission)) {
            return true;
        }
        return false;
    }

    private void requestPermission(Activity thisActivity, String[] myPermissions, int myPermissionRequestCode){
        ActivityCompat.requestPermissions(thisActivity,myPermissions,myPermissionRequestCode);
    }


    private List<String> getAllNeededPermissions(Context context){
        List<String> permissions = new ArrayList<String>();
        SharedPreferences sharedPref;
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        //Acceleration //Barometer //Gyro //Compass
        if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_ACCELERATION, true)
                | sharedPref.getBoolean (SettingsActivity.KEY_PREF_BAROMETER, true)
                | sharedPref.getBoolean (SettingsActivity.KEY_PREF_GYRO, true)
                | sharedPref.getBoolean (SettingsActivity.KEY_PREF_COMPASS, true)
                | true
                ){
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        //GPS
        if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_GPS, true)){
           permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        //Video
        if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_VIDEO, true)){
            permissions.add(Manifest.permission.CAMERA);
        }
        //Audio
        if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_AUDIO, true)){
            permissions.add(Manifest.permission.RECORD_AUDIO);
        }

        return permissions;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }


}

