package ch.bfh.students.schmm11.androidblackboxprototype.Sensor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import ch.bfh.students.schmm11.androidblackboxprototype.MainActivity;
import ch.bfh.students.schmm11.androidblackboxprototype.RealTimeTransfer.RealTimeSender;
import ch.bfh.students.schmm11.androidblackboxprototype.Saver.CrashSaver;
import ch.bfh.students.schmm11.androidblackboxprototype.Settings.SettingsActivity;

public class CrashDetectorListener implements SensorEventListener, SensorInterface {
    private Sensor sensorPressure, sensorAcceleration;
    private CrashSaver crashSaver = new CrashSaver();
    private static final String TAG = "BlackBox_CrashDetection";
    private SensorManager mSensorManager;
    private long totalCrashAmount = 0;
    private final Context context;

    private Double actualGForce = 0.0;    // values from acceleration sensor are double
    private Double gForceAlertValue;      // get later from settings

    private Float actualPressure = 0f;    // values from barometer sensor are in float
    private Float previousPressure = 0f;
    private Float pressureAlertValue;     // get later from settings

    private Float gyroscopeAlertValue;

    private SharedPreferences sharedPref;

    // Broacdast String
    public static final String BROADCAST_ACTION = "ALARM_INTENT";


    public CrashDetectorListener(Context context) {
        this.context = context;
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        //set alert values:
        gForceAlertValue = Double.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_GFORCETHRESHOLD, "3.0"));
        pressureAlertValue = Float.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_AIRBAGTHRESHOLD, "1.5"));
        gyroscopeAlertValue = Float.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_GYROSCOPETHRESHOLD, "1.5"));

        }

    @Override
    public void onSensorChanged(SensorEvent event) {


    // Beschleunigung
        if(event.sensor.getType()== Sensor.TYPE_LINEAR_ACCELERATION) {

            // Calculate directionless GForce: math.sqrt (x^2 + y^2 + z^2 )/9.8
            actualGForce = Math.sqrt(event.values[0] * event.values[0] + event.values[1] * event.values[1] + event.values[2] * event.values[2]) / 9.81;

            //When delta is bigger than x => Alarm
            if (actualGForce > gForceAlertValue ) {
                Log.d(TAG, "Alarm: Beschleunigung Delta ist: " + String.format("%.2f", actualGForce) + "Gesamtanzahl Crashes: " + totalCrashAmount);
                totalCrashAmount++;
                sendAlarm();
                crashSaver.save(System.currentTimeMillis() + "; Beschleunigung; x:" + event.values[0] + ";y:" + event.values[1] + ";z:" + event.values[2] + ";actualgForce:"+ actualGForce + System.getProperty ("line.separator"));
            }

        }
        // Luftdruck
        if(event.sensor.getType()== Sensor.TYPE_PRESSURE) {
            actualPressure = event.values[0];
            float deltaPressure = 0;
            if (previousPressure != 0) {
                deltaPressure = actualPressure - previousPressure;
            }
            if (Math.abs(deltaPressure) > pressureAlertValue) {
                Log.d(TAG, "Alarm. Lufdruck Delta "+ deltaPressure+ " Gesamtzahl Crashes:");
                totalCrashAmount++;
                sendAlarm();
                crashSaver.save(System.currentTimeMillis() + "; Luftdruck; Delta:" + deltaPressure + ";Pressure:"+ actualPressure + System.getProperty ("line.separator"));
            }
            previousPressure = actualPressure;
        }
}

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void start() {
        sensorPressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        mSensorManager.registerListener((SensorEventListener) this, sensorPressure, SensorManager.SENSOR_DELAY_GAME);
        sensorAcceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mSensorManager.registerListener((SensorEventListener) this, sensorAcceleration, SensorManager.SENSOR_DELAY_GAME);
        crashSaver.save("Timestamp [ms]; Reason; Data; Data; Data"+ System.getProperty ("line.separator")); //headlines

    }
    @Override
    public void stop() {
        mSensorManager.unregisterListener(this, sensorPressure);
        mSensorManager.unregisterListener(this, sensorAcceleration);
    }

    @Override
    public void restart() {
        // Do not restart this class! => we do nothing
    }

    /*
    This method is called when a sensor exceed the threshold
     */

    private void sendAlarm(){
        // alarm the Main Activity
        Intent intent = new Intent(BROADCAST_ACTION);
        Log.d(TAG, "sending broadcast");

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);


    }

}
