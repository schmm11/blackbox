package ch.bfh.students.schmm11.androidblackboxprototype.Sensor;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import ch.bfh.students.schmm11.androidblackboxprototype.Saver.AccelerationSaver;

public class AccelerationSensorListener implements SensorEventListener, SensorInterface {
    private Sensor sensor;
    private AccelerationSaver saver = new AccelerationSaver();
    private static final String TAG = "Blackbox_AccListener";
    private SensorManager mSensorManager;
    private float[] mAccelGravityData = new float[3];
    private String strToSave ="";
    private long amount;
    private Boolean isRealTimeEnabled;
    private final String REALTIMETRANSFER_INTENT = "REALTIMETRANSFER_INTENT";
    private LocalBroadcastManager localBroadcastManager;

    public AccelerationSensorListener(Context context, Boolean realTimeTransfer){
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        isRealTimeEnabled = realTimeTransfer;

        localBroadcastManager = LocalBroadcastManager.getInstance(context);
    }



    @Override
    public void onSensorChanged(SensorEvent event) {
        Log.d(TAG, "Acc SensorData received");
        long timestamp = System.currentTimeMillis();
        this.strToSave += timestamp + ";" + event.timestamp + ";" + event.values[0] + ";" + event.values[1] + ";" + event.values[2] + System.getProperty ("line.separator");
        amount++;

        /*
        Only call save() every n time => sonst verhacken sich die Save Befehle
         */
        if(amount % 6 == 0) {
            saver.save(strToSave);
            strToSave ="";
            Log.d(TAG, "onSensorChangedisCalled amount: " + amount);
        }

                /*
        Send an Broadcast to the MainActivity
         */
        if (isRealTimeEnabled) {
            Intent localIntent = new Intent(REALTIMETRANSFER_INTENT);
            localIntent.putExtra("data", "acc;" + strToSave);
            localBroadcastManager.sendBroadcast(localIntent);
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void start() {
        mSensorManager.registerListener((SensorEventListener) this, sensor, SensorManager.SENSOR_DELAY_GAME);
        saver.save("Timestamp [ms]; Event-Timestamp [ns]; X-axis [m/s^2]; Y-axis [m/s^2]; Z-axis [m/s^2]" + System.getProperty ("line.separator")); //headlines
    }

    @Override
    public void stop() {
        mSensorManager.unregisterListener(this, sensor);
    }

    @Override
    public void restart() {
        saver = new AccelerationSaver();
        stop();
        start();
    }
}
