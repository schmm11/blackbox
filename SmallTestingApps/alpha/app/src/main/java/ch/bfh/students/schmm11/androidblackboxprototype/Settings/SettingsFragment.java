package ch.bfh.students.schmm11.androidblackboxprototype.Settings;


import android.content.RestrictionEntry;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import ch.bfh.students.schmm11.androidblackboxprototype.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragmentCompat {

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreatePreferences(Bundle savedInstanceState,
                                    String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);

        final ListPreference listCamera = (ListPreference) findPreference(SettingsActivity.KEY_PREF_CAMERA);
        final ListPreference listResolution = (ListPreference) findPreference(SettingsActivity.KEY_PREF_RESOLUTION);
        final SwitchPreferenceCompat videoSwitch = (SwitchPreferenceCompat) findPreference(SettingsActivity.KEY_PREF_VIDEO);
        final SwitchPreferenceCompat audioSwitch = (SwitchPreferenceCompat) findPreference(SettingsActivity.KEY_PREF_AUDIO);
        final EditTextPreference loopTimeText = (EditTextPreference) findPreference(SettingsActivity.KEY_PREF_LOOPLENGTH);
        final EditTextPreference partIntervalText = (EditTextPreference) findPreference(SettingsActivity.KEY_PREF_PARTLENGTH) ;
        final EditTextPreference gForceThresholdText = (EditTextPreference) findPreference(SettingsActivity.KEY_PREF_GFORCETHRESHOLD);
        final EditTextPreference airbagThresholdText = (EditTextPreference) findPreference(SettingsActivity.KEY_PREF_AIRBAGTHRESHOLD);
        final EditTextPreference gyroscopeThresholdText = (EditTextPreference) findPreference(SettingsActivity.KEY_PREF_GYROSCOPETHRESHOLD);


        //  set list entries
        setCameraList(listCamera);
        setResolutionList(listResolution, Integer.valueOf(listCamera.getValue()));

        // choose resolution if nothing is selected
        if(listResolution.getEntry() == ""){
            listResolution.setDefaultValue("1280x720");
        }

        // update all summaries
        listCamera.setSummary(listCamera.getEntry());
        listResolution.setSummary(listResolution.getEntry());
        loopTimeText.setSummary(loopTimeText.getText());
        partIntervalText.setSummary(partIntervalText.getText());
        gForceThresholdText.setSummary(gForceThresholdText.getText());
        airbagThresholdText.setSummary(airbagThresholdText.getText());
        gyroscopeThresholdText.setSummary(gyroscopeThresholdText.getText());


        // update audio switch
        if(!videoSwitch.isChecked()){
            audioSwitch.setChecked(false);
            audioSwitch.setEnabled(false);
        }

        // updates summary text loopTime
        loopTimeText.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                loopTimeText.setSummary(newValue.toString());
                return true;
            }
        }
        );

        // updates summary text partInterval
        partIntervalText.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                partIntervalText.setSummary(newValue.toString());
                return true;
            }
        });

        // updates summary text of Cameralist
        listCamera.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    int index = listCamera.findIndexOfValue(newValue.toString());  // get index of the actual selected Value
                    CharSequence[] cameras = listCamera.getEntries();               // get all Entrys
                    listCamera.setSummary(cameras[index]);                          // set Entry with the correct index to summary

                    setResolutionList(listResolution, Integer.valueOf(newValue.toString()));
                    return true;
                }
        });

        // updates summary text of Resolutionlist (like above)
        listResolution.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                int index = listResolution.findIndexOfValue(newValue.toString());
                CharSequence[] resolutions = listResolution.getEntries();
                listResolution.setSummary(resolutions[index]);
                return true;
            }
        });

        //activate deactivate audio switch
        videoSwitch.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                    audioSwitch.setChecked((Boolean) newValue);
                    audioSwitch.setEnabled((Boolean) newValue);
                    return true;
            }
        });

        // updates summary text gForceTreshold
        gForceThresholdText.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                                                       @Override
                                                       public boolean onPreferenceChange(Preference preference, Object newValue) {
                                                           gForceThresholdText.setSummary(newValue.toString());
                                                           return true;
                                                       }
                                                   }
        );
        // updates summary text airbagTresholdText
        airbagThresholdText.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                                                             @Override
                                                             public boolean onPreferenceChange(Preference preference, Object newValue) {
                                                                 airbagThresholdText.setSummary(newValue.toString());
                                                                 return true;
                                                             }
                                                         }
        );
        // updates summary text gyroscopeTresholdText
        gyroscopeThresholdText.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                                                             @Override
                                                             public boolean onPreferenceChange(Preference preference, Object newValue) {
                                                                 gyroscopeThresholdText.setSummary(newValue.toString());
                                                                 return true;
                                                             }
                                                         }
        );


    }



    private void setCameraList(ListPreference lp){
        int numCams = Camera.getNumberOfCameras();
        List<String> listEntries = new ArrayList<>();
        List<String> listEntryValues = new ArrayList<>();
        CharSequence[] entries;
        CharSequence[] entryValues;

        for(int i=0; i< numCams; i++){
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing==Camera.CameraInfo.CAMERA_FACING_BACK){
                listEntries.add("Back camera");
                listEntryValues.add(String.valueOf(i));
            }else if(cameraInfo.facing==Camera.CameraInfo.CAMERA_FACING_FRONT){
                listEntries.add("Front camera");
                listEntryValues.add(String.valueOf(i));
            }else{
                listEntries.add("External camera");
                listEntryValues.add(String.valueOf(i));
            }
            entries = listEntries.toArray(new CharSequence[0]);
            entryValues = listEntryValues.toArray(new CharSequence[0]);
            lp.setEntries(entries);
            lp.setEntryValues(entryValues);
        }
    }

    private void setResolutionList(ListPreference lp) {
        CharSequence[] entries = {"1920x1080", "1280x720", "720x480"};
        CharSequence[] entryValues = {"1920x1080", "1280x720", "720x480"};
        lp.setEntries(entries);
        lp.setDefaultValue("1080x720");
        lp.setEntryValues(entryValues);
    }

    private void setResolutionList(ListPreference lp, Integer camID){
        List<String> listEntries = new ArrayList<>();
        List<String> listEntryValues = new ArrayList<>();
        CharSequence[] entries;
        CharSequence[] entryValues;
        List<Camera.Size> resolutions = getCameraResolutions(camID);
        if(resolutions!=null) {
            for(Camera.Size resolution: resolutions){
                listEntries.add(resolution.width + " x " + resolution.height);
                listEntryValues.add(resolution.width + "x" + resolution.height);
            }
            entries = listEntries.toArray(new CharSequence[0]);
            entryValues = listEntryValues.toArray(new CharSequence[0]);
            lp.setEntries(entries);
            lp.setEntryValues(entryValues);
        }else{
            setResolutionList(lp);
        }
    }

    private List<Camera.Size> getCameraResolutions(int camID){
        Camera mCamera = null;
        try{
            mCamera = Camera.open(camID);
            Camera.Parameters p = mCamera.getParameters();
            List<Camera.Size> resolutions = p.getSupportedVideoSizes();
            mCamera.release();
            return resolutions;
        } catch (Exception e){
            try{
                if(mCamera  != null) {
                    Log.e("Camera (try to release)", e.getMessage());
                    mCamera.release();
                }
            }catch (Exception ex){
                Log.e("CameraGetResolutions", ex.getMessage());
            }
        }
        return null;
    }
}