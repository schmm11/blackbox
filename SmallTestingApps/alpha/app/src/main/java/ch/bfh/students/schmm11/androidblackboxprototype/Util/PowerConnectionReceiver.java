package ch.bfh.students.schmm11.androidblackboxprototype.Util;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import ch.bfh.students.schmm11.androidblackboxprototype.MainActivity;
import ch.bfh.students.schmm11.androidblackboxprototype.Settings.SettingsActivity;

import static android.support.v4.content.ContextCompat.startActivity;


//Todo: start application if charging. see: https://developer.android.com/guide/components/broadcasts

//Todo: Fuckoff! https://commonsware.com/blog/2017/04/11/android-o-implicit-broadcast-ban.html
// 2018-10-22 14:53:03.537 5213-8828/? W/BroadcastQueue: Background execution not allowed: receiving Intent { act=android.intent.action.ACTION_POWER_CONNECTED flg=0x4000010 (has extras) } to ch.bfh.students.schmm11.androidblackboxprototype/.PowerConnectionReceiver
//2018-10-22 14:53:04.141 5213-7205/? W/BroadcastQueue: Background execution not allowed: receiving Intent { act=android.intent.action.ACTION_POWER_DISCONNECTED flg=0x4000010 } to ch.bfh.students.schmm11.androidblackboxprototype/.PowerConnectionReceiver
//2018-10-22 14:53:04.642 5213-7205/? W/BroadcastQueue: Background execution not allowed: receiving Intent { act=android.intent.action.ACTION_POWER_CONNECTED flg=0x4000010 (has extras) } to ch.bfh.students.schmm11.androidblackboxprototype/.PowerConnectionReceiver
//2018-10-22 14:53:05.507 5213-7760/? W/BroadcastQueue: Background execution not allowed: receiving Intent { act=android.intent.action.ACTION_POWER_DISCONNECTED flg=0x4000010 } to ch.bfh.students.schmm11.androidblackboxprototype/.PowerConnectionReceiver



public class PowerConnectionReceiver extends BroadcastReceiver {
    private static final String TAG = "PowerConnectionReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        if ( sharedPref.getBoolean (SettingsActivity.KEY_PREF_AUTOSTART, false)) {

            Toast.makeText(context, "blabla pöwer ön", Toast.LENGTH_LONG).show();
            Intent myIntent = new Intent(context, MainActivity.class);
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(context, myIntent, null);

        }

/*        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

        if(isCharging){

        }

        if(usbCharge){
            Toast.makeText(context, "The device get charged by USB", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "the device get charged by USB");
        }
        if(acCharge){
            Toast.makeText(context, "The device get charged by AC supply", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "the device get charged by AC");
        }*/

    }
}
