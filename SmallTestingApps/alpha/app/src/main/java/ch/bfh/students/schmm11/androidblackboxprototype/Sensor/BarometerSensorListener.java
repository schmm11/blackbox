package ch.bfh.students.schmm11.androidblackboxprototype.Sensor;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import ch.bfh.students.schmm11.androidblackboxprototype.MainActivity;
import ch.bfh.students.schmm11.androidblackboxprototype.R;
import ch.bfh.students.schmm11.androidblackboxprototype.Saver.AccelerationSaver;
import ch.bfh.students.schmm11.androidblackboxprototype.Saver.BarometerSaver;
import ch.bfh.students.schmm11.androidblackboxprototype.Settings.SettingsActivity;

public class BarometerSensorListener implements SensorEventListener, SensorInterface {
    private Sensor sensor;
    private BarometerSaver saver = new BarometerSaver();
    private static final String TAG = "Blackbox_BarListener";
    private SensorManager mSensorManager;
    private String strToSave = "";
    private long amount;
    private final String REALTIMETRANSFER_INTENT = "REALTIMETRANSFER_INTENT";
    private LocalBroadcastManager localBroadcastManager;
    private Boolean isRealTimeEnabled;

    public BarometerSensorListener(Context context, Boolean realTimeTransfer){
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);

        isRealTimeEnabled = realTimeTransfer;

        localBroadcastManager = LocalBroadcastManager.getInstance(context);
    }



    @Override
    public void onSensorChanged(SensorEvent event) {
        Log.d(TAG, "Barometer SensorData received");
        long timestamp = System.currentTimeMillis();

        // @TODO Get a good value from the event

        // pressure is stored in values[0] as [hPa]

        // Just for test reasons: we can get the altitude from pressure
        //String altitude = String.valueOf(SensorManager.getAltitude(SensorManager.PRESSURE_STANDARD_ATMOSPHERE, event.values[0]));
        //this.strToSave += "TS:"+ timestamp + "; value:" + event.values[0] +";" + altitude + System.getProperty ("line.separator");

        this.strToSave += timestamp + ";" + event.timestamp + ";" + event.values[0] +  System.getProperty ("line.separator");


        amount++;

        /*
        Only call save() every n time => sonst verhacken sich die Save Befehle
         */
        if(amount % 6 == 0) {
            saver.save(strToSave);
            strToSave ="";
            Log.d(TAG, "onSensorChangedisCalled amount: " + amount);
        }

        /*
        Send an Broadcast to the MainActivity
         */
        if (isRealTimeEnabled) {
            Intent localIntent = new Intent(REALTIMETRANSFER_INTENT);
            localIntent.putExtra("data", "bar;" + strToSave);
            localBroadcastManager.sendBroadcast(localIntent);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void start() {
        mSensorManager.registerListener((SensorEventListener) this, sensor, SensorManager.SENSOR_DELAY_GAME);
        saver.save("Timestamp [ms]; Event-Timestamp [ns]; Pressure [mbar]" + System.getProperty ("line.separator")); //headlines
    }

    @Override
    public void stop() {
        mSensorManager.unregisterListener(this, sensor);
    }

    @Override
    public void restart() {
        saver = new BarometerSaver();
        stop();
        start();
    }
}
