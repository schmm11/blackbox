package ch.bfh.students.schmm11.androidblackboxprototype.Sensor;

public interface SensorInterface {

public void start();

public void stop();

public void restart();

}
