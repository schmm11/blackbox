package ch.bfh.students.schmm11.androidblackboxprototype.Services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import ch.bfh.students.schmm11.androidblackboxprototype.Settings.SettingsActivity;


public class DeleteService extends Service {
    private static final String TAG = "DeleterService";

    SharedPreferences sharedPref;
    int loopTime;
    Date startDate;
    Date loopTimeDate;
    Handler deleteHandler = new Handler();




    @Nullable
    @Override
    public IBinder onBind(Intent intent) {return null;}


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        startDate = Calendar.getInstance().getTime();

        String loopTimeString = sharedPref.getString(SettingsActivity.KEY_PREF_LOOPLENGTH, "10");            // Preferences are String. No chance to change :(
        loopTime = Integer.valueOf(loopTimeString);

        loopTimeDate = new Date(0,0,0,0,loopTime);

        deleteHandler.post(deleteOutdatedFiles);

        return super.onStartCommand(intent, flags, startId);
    }


    //Class for restarting videorecording
    private Runnable deleteOutdatedFiles = new Runnable() {
        @Override
        public void run() {

            try {
                File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/AndroidBlackbox");

                //           | Modified previus |       |start<=====|modified=======|modified====DELETE SECTION========>(------------------loopTime---------|now)

                if(file.isDirectory()){
                    for(File child : file.listFiles()){
                        if(!child.isDirectory()){
                            Date lastModDate = new Date(child.lastModified());
                            long lastModTimeMillis = lastModDate.getTime();
                            long startDateTimeMillis = startDate.getTime();
                            long nowTimeMillis = Calendar.getInstance().getTimeInMillis();
                            long loopTimeMillis = loopTime * 60000; //looptime is a value in minutes
                            long deleteOlderThan = nowTimeMillis - loopTimeMillis;
                            long deleteYoungerThan = startDateTimeMillis;

                            if(lastModTimeMillis > deleteYoungerThan && lastModTimeMillis < deleteOlderThan){
                                child.delete();
                                Log.d(TAG, "Deleted: " + child.getPath());
                                Toast.makeText(getApplicationContext(), "Deleted: " + child.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                }

            }catch (Exception e){
                Log.e(TAG, e.getMessage());
            }







            int interval = 5000;
            deleteHandler.postDelayed(deleteOutdatedFiles,interval);
        }
    };



    @Override
    public void onDestroy() {
        //clean a last time then remove callbacks
        deleteHandler.post(deleteOutdatedFiles);
        deleteHandler.removeCallbacks(deleteOutdatedFiles);
        super.onDestroy();
    }
}
