package ch.bfh.ti.bachelor.foregroundservicetest;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

public class RecordingService extends Service {
    private static final String TAG = "RecordingService";
    private SurfaceView mSurfaceView;
    private SurfaceHolder mSurfaceHolder;
    private static Camera mServiceCamera;
    private boolean mRecordingStatus;
    private MediaRecorder mMediaRecorder;
    private  Camera.Parameters cameraParameters;

    @Override
    public void onCreate() {
        super.onCreate();
        mSurfaceView = MainActivity.mSurfaceView; //do we need this?
        mSurfaceHolder = MainActivity.mSurfaceHolder;
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals("ch.bfh.ti.bachelor.foregroundservicetest.action.startforeground")) {
            Log.i(TAG, "Received Start Foreground Intent ");
            Intent notificationIntent = new Intent(this, MainActivity.class);
            notificationIntent.setAction("ch.bfh.ti.bachelor.foregroundservicetest.action.main");
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

            Intent startRecordingIntent = new Intent(this, RecordingService.class);
            startRecordingIntent.setAction("ch.bfh.ti.bachelor.foregroundservicetest.action.startrecording");
            PendingIntent pStartRecordingIntent = PendingIntent.getService(this, 0, startRecordingIntent, 0);

            Intent stopRecordingIntent = new Intent(this, RecordingService.class);
            stopRecordingIntent.setAction("ch.bfh.ti.bachelor.foregroundservicetest.action.stoprecording");
            PendingIntent pStopRecordingIntent = PendingIntent.getService(this, 0, stopRecordingIntent, 0);

            /*
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                prepareChannel(getApplicationContext(), "1", NotificationManagerCompat.IMPORTANCE_MAX);
            }
            */
            //Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_background);




            Notification notification =   new NotificationCompat.Builder(this)
                    .setContentTitle("titel")
                    .setTicker("Ticker")
                    .setContentText("inhalt")
                    .setSmallIcon(R.drawable.ic_launcher_background)


                    //.setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                    .setContentIntent(pendingIntent)

                    .setOngoing(true)

                    .addAction(android.R.drawable.ic_menu_camera,"Start Recording", pStartRecordingIntent)
                    .addAction(android.R.drawable.ic_media_pause, "Stop Recording", pStopRecordingIntent)


                    .build();



            startForeground(100, notification);
            startRecording();
            Toast.makeText(this, "service started!" , Toast.LENGTH_LONG).show();



        } else if (intent.getAction().equals("ch.bfh.ti.bachelor.foregroundservicetest.action.startrecording")) {


            Toast.makeText(this, "clicked startrecording!" , Toast.LENGTH_LONG).show();
            startRecording();


        } else if (intent.getAction().equals("ch.bfh.ti.bachelor.foregroundservicetest.action.stoprecording")) {


            Toast.makeText(this, "clicked stoprecording!" , Toast.LENGTH_LONG).show();
            stopRecording();

        } else if (intent.getAction().equals("ch.bfh.ti.bachelor.foregroundservicetest.action.stopforeground")) {

            Toast.makeText(this, "clicked stop foreground service!" , Toast.LENGTH_LONG).show();
            stopRecording();
            stopForeground(true);
            stopSelf();
        } else if (intent.getAction().equals("ch.bfh.ti.bachelor.foregroundservicetest.action.renewpreview")){

            Toast.makeText(this, "preview changed!" , Toast.LENGTH_SHORT).show();
            if (mRecordingStatus) {
                mSurfaceHolder = MainActivity.mSurfaceHolder;
                mSurfaceView = MainActivity.mSurfaceView;

                Toast.makeText(this, "Surfaceview: " + mSurfaceView.toString() , Toast.LENGTH_LONG).show();

                mServiceCamera.stopPreview();
                setPreview();
                mMediaRecorder.setPreviewDisplay(MainActivity.mSurfaceHolder.getSurface());


                /*
                mServiceCamera.setPreviewCallback(new Camera.PreviewCallback() {
                @Override
                public void onPreviewFrame(byte[] bytes, Camera camera) {

                }
            });*/


            }
        }



        return START_STICKY;
    }





    private boolean startRecording(){
        try {
            if (prepareCamera()){
                if(setPreview()){
                    mServiceCamera.unlock();
                    mMediaRecorder = new MediaRecorder();
                    if(configureMediaRecorder()){
                        mMediaRecorder.prepare();
                        mMediaRecorder.start();
                        mRecordingStatus = true;
                    }
                }
            }
            //Toast.makeText(getBaseContext(), String.format("Selected Size (%d x %d)", cameraParameters.getPreviewSize().width, cameraParameters.getPreviewSize().height), Toast.LENGTH_SHORT).show();

            return true;

        } catch (IllegalStateException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
            return false;

        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
            return false;
        }
    }



    //todo: describe method
    private boolean prepareCamera(){
        try {
            String camID = "1";
            String resolution = "1920x1080";
            String resolutionWidth = resolution.substring(0, resolution.indexOf("x"));                                        // stored in the Preference like "1920x1080"
            String resolutionHeight = resolution.substring(resolution.indexOf("x") + 1, resolution.length());

            mServiceCamera = Camera.open(Integer.valueOf(camID));

            cameraParameters = mServiceCamera.getParameters();

            final List<Camera.Size> listPreviewSize = cameraParameters.getSupportedPreviewSizes();
            for (Camera.Size size : listPreviewSize) {

                if (size.height == Integer.valueOf(resolutionHeight) & size.width == Integer.valueOf(resolutionWidth)) {
                    cameraParameters.setPreviewSize(size.width, size.height);
                }
                Log.d(TAG, String.format("Supported Preview Size (%d, %d)", size.width, size.height));
            }
            cameraParameters.setRecordingHint(true);
            mServiceCamera.setParameters(cameraParameters);
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
            return false;
        }
        return true;
    }

    //todo: describe method
    private boolean setPreview(){
        try {
            mServiceCamera.setPreviewDisplay(mSurfaceHolder);
            mServiceCamera.startPreview();
        }
        catch (IOException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    //todo: describe method
    private boolean configureMediaRecorder(){
        try{
            mMediaRecorder.setCamera(mServiceCamera);

                mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);

            mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);

                mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

            mMediaRecorder.setVideoEncodingBitRate(10000000);
            mMediaRecorder.setVideoSize(cameraParameters.getPreviewSize().width, cameraParameters.getPreviewSize().height);
            mMediaRecorder.setOutputFile(getVideoFilePath().getPath());
            mMediaRecorder.setPreviewDisplay(mSurfaceHolder.getSurface());


                int partLengthSeconds = 30;
                mMediaRecorder.setMaxDuration(partLengthSeconds*1000);
                mMediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
                    @Override
                    public void onInfo(MediaRecorder mr, int what, int extra) {
                        if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                            restartVideoRecording();
                        }
                    }
                });


        }catch (Exception e){
            Log.e(TAG,e.getMessage());
            return false;
        }
        return true;
    }


    //todo: describe method
    private void restartVideoRecording() {
        mMediaRecorder.stop();
        mMediaRecorder.reset();

        if (setPreview()) {
            mServiceCamera.unlock();
            mMediaRecorder = new MediaRecorder();
            if (configureMediaRecorder())
            {
                try {
                    mMediaRecorder.prepare();
                    mMediaRecorder.start();
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        }
    }

    //todo: describe method
    public void stopRecording() {
        try {
            mServiceCamera.reconnect();

        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mMediaRecorder.stop();
            mMediaRecorder.reset();

            mServiceCamera.stopPreview();
            mMediaRecorder.release();

            mServiceCamera.release();
            mServiceCamera = null;
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }
    }



    private File getVideoFilePath() {

        final File filepath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/AndroidBlackboxForegroundServiceTest");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HH-mm-ss");


        if (!filepath.exists()) {
            filepath.mkdirs();
        }
        File file = new File(filepath.getPath(), sdf.format(System.currentTimeMillis()) + "_Video.mp4");

        return file;
    }





    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy of RecordingService is called");
    }




    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
