package ch.bfh.students.schmm11.nearbykamera_recorder;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback {
    private Button btnAdvertise, btnRecord;
    private boolean mIsAdvertising = false;
    private boolean mIsConnected = false;
    private boolean mIsRecording = false;
    private static final String TAG = "KameraRecorder_Main";

    //Kamera
    public static SurfaceView mSurfaceView;
    public static SurfaceHolder mSurfaceHolder;
    public static Camera mCamera;

    //Nearby
    private Strategy strategy = Strategy.P2P_POINT_TO_POINT;
    private String ownEndpointname, partnerEndpointName;
    private String serviceId = "ch.bfh.students.schmm11.nearby_kamera";
    private ConnectionsClient mConnectionsClient;



    private final ConnectionLifecycleCallback mConnectionLifecycleCallback =
            new ConnectionLifecycleCallback() {
                @Override
                public void onConnectionInitiated(String endpointId, ConnectionInfo connectionInfo) {
                    // Automatically accept the connection on both sides.
                    partnerEndpointName = endpointId;
                    Log.d(TAG, "onConnectionInit from" + endpointId);
                    mConnectionsClient.acceptConnection(endpointId, mPayloadCallback);
                }

                @Override
                public void onConnectionResult(String endpointId, ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode()) {
                        case ConnectionsStatusCodes.STATUS_OK:
                            Log.d(TAG, "We're sucessfully Nearby connected!");
                            mIsConnected = true;
                            btnAdvertise.setText("Disconnect");
                            break;
                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            Log.d(TAG, "The connection was rejected by one or both sides");
                            break;
                        default:
                            Log.d(TAG, "The connection was broken before it was accepted.");
                            break;
                    }
                }
                @Override
                public void onDisconnected(String endpointId) {
                    mIsConnected = false;
                    Log.d(TAG, "We're disconnected");
                    btnAdvertise.setText("Disconnect");
                }
            };

    //receive a Payload
    private final PayloadCallback mPayloadCallback =
            new PayloadCallback() {
                @Override
                public void onPayloadReceived(String endpointId, Payload payload) {
                    //Log.d(TAG, "Got a Payload from " + endpointId + ", but we should receive something....");
                }
                @Override
                public void onPayloadTransferUpdate(String endpointId, PayloadTransferUpdate update) {
                    if (update.getStatus() == PayloadTransferUpdate.Status.SUCCESS) {
                        //Log.d(TAG, "Got a Payload Update from " + endpointId + ", but we should receive something....");
                    }
                }
            };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mConnectionsClient = Nearby.getConnectionsClient(this);

        //SurfaceView
        mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        //Buttons
        btnAdvertise = (Button) findViewById(R.id.btnAdvertise);
        btnRecord = (Button) findViewById(R.id.btnRecord);

        btnAdvertise.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                //only discover when not allready Advertise or Discovering
                if(!mIsAdvertising){
                    startAdvertising();
                    btnAdvertise.setText("Is advertising...");
                }
                else if(mIsAdvertising && mIsConnected){
                    stopAdvertising();
                    btnAdvertise.setText("Stopped Advertisign");
                }
            }
        });

        btnRecord.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if(!mIsRecording) {
                    Intent intent = new Intent(MainActivity.this, RecorderService.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startService(intent);
                    mIsRecording = !mIsRecording;
                    btnRecord.setText("Stop recording");
                }
                else{
                    stopService(new Intent(MainActivity.this, RecorderService.class));
                    mIsRecording = !mIsRecording;
                    btnRecord.setText("Start Recording");
                }
            }
        });
    }






    /*
    Nearby Methods
     */
    private void startAdvertising() {

        mIsAdvertising = true;
        mConnectionsClient
                .startAdvertising(
                        /* endpointName= */ "AdvertiserDevice",
                        /* serviceId= */ serviceId,
                        mConnectionLifecycleCallback,
                        new AdvertisingOptions(strategy))
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unusedResult) {
                                Log.d(TAG,"Now Advertising....");
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                mIsAdvertising = false;
                                Log.d(TAG,"startAdvertising failed e: " +e);
                            }
                        });
    }

    private void stopAdvertising() {
        mIsAdvertising = false;
        mConnectionsClient.stopAdvertising();
    }

    private void sendData(Payload payload) {
        mConnectionsClient.sendPayload(partnerEndpointName, payload)
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "send Payload failed; e:" + e);
                            }
                        });
    }

    /*
    Camera methods
     */
    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

}