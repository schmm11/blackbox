package ch.bfh.students.schmm11.v2nearbyrecorder;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.FileNotFoundException;

import ir.mahdi.mzip.zip.ZipArchive;

public class NearbySender {
    private String TAG = "EmergencySender";

    //File
    private String sourcePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)+"/AndroidBlackbox/";
    private String destinationPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)+"/AndroidBlackboxBackup.zip";

    //Nearby
    private Strategy strategy = Strategy.P2P_POINT_TO_POINT;
    private String ownEndpointname, partnerEndpointName;
    private String serviceId = "ch.bfh.students.schmm11.emergency";
    private ConnectionsClient mConnectionsClient;

    //Nearby ConnectionLifeCycle
    private final ConnectionLifecycleCallback mConnectionLifecycleCallback =
            new ConnectionLifecycleCallback() {
                @Override
                public void onConnectionInitiated(String endpointId, ConnectionInfo connectionInfo) {
                    // Automatically accept the connection on both sides.
                    partnerEndpointName = endpointId;
                    Log.d(TAG, "onConnectionInit from" + endpointId);
                    mConnectionsClient.acceptConnection(endpointId, mPayloadCallback);
                }

                @Override
                public void onConnectionResult(String endpointId, ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode()) {
                        case ConnectionsStatusCodes.STATUS_OK:
                            Log.d(TAG, "We're sucessfully Nearby connected!");
                            startSendingData();
                            break;
                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            Log.d(TAG, "The connection was rejected by one or both sides");
                            break;
                        default:
                            Log.d(TAG, "The connection was broken before it was accepted.");
                            break;
                    }
                }
                @Override
                public void onDisconnected(String endpointId) {
                    Log.d(TAG, "We're disconnected");
                }
            };

    //receive a Payload
    private final PayloadCallback mPayloadCallback =
            new PayloadCallback() {
                @Override
                public void onPayloadReceived(String endpointId, Payload payload) {
                    //Log.d(TAG, "Got a Payload from " + endpointId + ", but we should receive something....");
                }
                @Override
                public void onPayloadTransferUpdate(String endpointId, PayloadTransferUpdate update) {
                    if (update.getStatus() == PayloadTransferUpdate.Status.SUCCESS) {
                        Log.d(TAG, "PayloadTransferUpdate success => are finished?");
                        // Shall we stop every Connection after 1 sucessfull download?
                        // mConnectionsClient.stopAllEndpoints();
                    }
                }
            };


    public NearbySender(Context context){
        //Maybe pass the context another way...
        mConnectionsClient = Nearby.getConnectionsClient(context);
    }

    /*
    This method is called from outside to start an hotspot
     */
    public void activateHotspot(){
        Log.d(TAG, "activateHotspot()");
        //First: Zip what we want
        prepareZip();
        //Second: Start Advertising
        startAdvertising();
    }

    /*
    This method is called when an sucessfull Nearby connection is made
     */
    private void startSendingData(){
        mConnectionsClient.sendPayload(partnerEndpointName, getZipFile());
    }

    /* Add following 2 things to buidl.gradle for MZIP:
        repositories {
            maven { url 'https://jitpack.io' }
        }
    And under "Dependencies":
        api 'com.github.ghost1372:Mzip-Android:0.4.0'
     */
    private void prepareZip() {
        Log.d(TAG, "preparezip();");
        ZipArchive zipArchive = new ZipArchive();
        zipArchive.zip(sourcePath, destinationPath,"12345");

    }


    private void startAdvertising() {
        mConnectionsClient
                .startAdvertising(
                        /* endpointName= */ "AdvertiserDevice",
                        /* serviceId= */ serviceId,
                        mConnectionLifecycleCallback,
                        new AdvertisingOptions(strategy))
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unusedResult) {
                                Log.d(TAG,"Now Advertising....");
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG,"startAdvertising failed e: " +e);
                            }
                        });
    }

    /*
    return the zip from the destination Path as Payload.
     */
    private Payload getZipFile() {
        File file = new File(destinationPath);
        Payload filePayload = null;
        try {
            filePayload = Payload.fromFile(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filePayload;
    }


}
