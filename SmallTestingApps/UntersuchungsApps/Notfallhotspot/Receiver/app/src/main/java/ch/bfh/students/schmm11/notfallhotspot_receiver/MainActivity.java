package ch.bfh.students.schmm11.notfallhotspot_receiver;

import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.util.SimpleArrayMap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo;
import com.google.android.gms.nearby.connection.DiscoveryOptions;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;


import java.io.File;
import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity {
    private Button btn;
    private TextView txt;

    private static final String TAG = "Notfallhotspot";
    private String destinationPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)+"/AndroidBlackboxBackup_received.zip";

    //Nearby
    private Strategy strategy = Strategy.P2P_POINT_TO_POINT;
    private String ownEndpointname, partnerEndpointName;
    private String serviceId = "ch.bfh.students.schmm11.notfall";
    private ConnectionsClient mConnectionsClient;


    private final SimpleArrayMap<Long, Payload> incomingPayloads = new SimpleArrayMap<>();
    private final SimpleArrayMap<Long, String> filePayloadFilenames = new SimpleArrayMap<>();


    private final ConnectionLifecycleCallback mConnectionLifecycleCallback =
            new ConnectionLifecycleCallback() {
                @Override
                public void onConnectionInitiated(String endpointId, ConnectionInfo connectionInfo) {
                    // Automatically accept the connection on both sides.
                    partnerEndpointName = endpointId;
                    Log.d(TAG, "onConnectionInit from" + endpointId);
                    mConnectionsClient.acceptConnection(endpointId, mPayloadCallback);
                }

                @Override
                public void onConnectionResult(String endpointId, ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode()) {
                        case ConnectionsStatusCodes.STATUS_OK:
                            Log.d(TAG, "We're sucessfully Nearby connected!");
                            //mIsConnected = true;
                            txt.append("Succesfull connect");
                            break;
                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            Log.d(TAG, "The connection was rejected by one or both sides");
                            break;
                        default:
                            Log.d(TAG, "The connection was broken before it was accepted.");
                            break;
                    }
                }
                @Override
                public void onDisconnected(String endpointId) {
                    //mIsConnected = false;
                    Log.d(TAG, "We're disconnected");
                    txt.append("Disconnect");
                }
            };


   // private final SimpleArrayMap<long, Payload> incomingPayloads = new SimpleArrayMap<>();


    //receive a Payload
    private final PayloadCallback mPayloadCallback =
            new PayloadCallback() {
                @Override
                public void onPayloadReceived(String endpointId, Payload payload) {
                    if (payload.getType() == Payload.Type.BYTES) {
                        String payloadFilenameMessage = null;
                        try {
                            payloadFilenameMessage = new String(payload.asBytes(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            Log.wtf(TAG, "Unssuported Encoding...");
                        }
                        addPayloadFilename(payloadFilenameMessage);
                    } else if (payload.getType() == Payload.Type.FILE) {
                        // Add this to our tracking map, so that we can retrieve the payload later.
                        incomingPayloads.put(payload.getId(), payload);
                    }
                }


                /**
                 * Extracts the payloadId and filename from the message and stores it in the
                 * filePayloadFilenames map. The format is payloadId:filename.
                 */
                private void addPayloadFilename(String payloadFilenameMessage) {
                    int colonIndex = payloadFilenameMessage.indexOf(":");
                    String payloadId = payloadFilenameMessage.substring(0, colonIndex);
                    String filename = payloadFilenameMessage.substring(colonIndex + 1);
                    filePayloadFilenames.put(Long.valueOf(payloadId), filename);
                }

                @Override
                public void onPayloadTransferUpdate(String endpointId, PayloadTransferUpdate update) {
                    if (update.getStatus() == PayloadTransferUpdate.Status.SUCCESS) {
                        long payloadId = update.getPayloadId();
                        Payload payload = incomingPayloads.remove(payloadId);
                        if (payload.getType() == Payload.Type.FILE) {
                            // Retrieve the filename that was received in a bytes payload.
                            String newFilename = filePayloadFilenames.remove(payloadId);

                            File payloadFile = payload.asFile().asJavaFile();

                            // Rename the file.
                            payloadFile.renameTo(new File(payloadFile.getParentFile(), newFilename));
                        }
                    }
                }

            };








    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //init layout
        setContentView(R.layout.activity_main);
        this.txt = (TextView) findViewById(R.id.txt);

        this.btn = (Button) findViewById(R.id.btn);
        btn.setText("unfall");
        btn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                startDiscovery();
            }
        });

        mConnectionsClient = Nearby.getConnectionsClient(this);
    }

    private void startDiscovery() {
        mConnectionsClient
                .startDiscovery(
                        serviceId,
                        new EndpointDiscoveryCallback() {
                            @Override
                            public void onEndpointFound(String endpointId, DiscoveredEndpointInfo info) {
                                Log.d(TAG, "Endpoint Found, Name is: "+ endpointId);
                                if (serviceId.equals(info.getServiceId())) {
                                    partnerEndpointName = endpointId;
                                    txt.append("Endpoint gefunden");

                                    // Ask for a Connection
                                    mConnectionsClient
                                            .requestConnection("DiscovererName", partnerEndpointName, mConnectionLifecycleCallback)
                                            .addOnFailureListener(
                                                    new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            Log.d(TAG, "onEndpointDiscovered failed");
                                                        }
                                                    });
                                }
                            }

                            @Override
                            public void onEndpointLost(String endpointId) {
                                Log.d(TAG, "onEndpointLost failed");
                                txt.append("Endppint verloren");
                            }
                        },
                        new DiscoveryOptions(strategy))
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unusedResult) {
                                Log.d(TAG, "Discovery started");
                                txt.append("Discovery started");
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                txt.append("Discovery failed");
                            }
                        });
    }
}

