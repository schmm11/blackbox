package ch.bfh.students.schmm11.notfallhotspot_recorder;

import android.content.Intent;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;

import ir.mahdi.mzip.zip.ZipArchive;

public class MainActivity extends AppCompatActivity {

    private Button btn;
    private TextView txt;

    private static final String TAG = "Notfallhotspot";
    private String sourcePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)+"/AndroidBlackbox/";
    private String destinationPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)+"/AndroidBlackboxBackup.zip";


    //Nearby
    private Strategy strategy = Strategy.P2P_POINT_TO_POINT;
    private String ownEndpointname, partnerEndpointName;
    private String serviceId = "ch.bfh.students.schmm11.notfall";
    private ConnectionsClient mConnectionsClient;

    private final ConnectionLifecycleCallback mConnectionLifecycleCallback =
            new ConnectionLifecycleCallback() {
                @Override
                public void onConnectionInitiated(String endpointId, ConnectionInfo connectionInfo) {
                    // Automatically accept the connection on both sides.
                    partnerEndpointName = endpointId;
                    Log.d(TAG, "onConnectionInit from" + endpointId);
                    mConnectionsClient.acceptConnection(endpointId, mPayloadCallback);
                }

                @Override
                public void onConnectionResult(String endpointId, ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode()) {
                        case ConnectionsStatusCodes.STATUS_OK:
                            Log.d(TAG, "We're sucessfully Nearby connected!");
                            //mIsConnected = true;
                            txt.append("Succesfull connect");

                            //send Payload
                            sendData(getZipFile());


                            break;
                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            Log.d(TAG, "The connection was rejected by one or both sides");
                            break;
                        default:
                            Log.d(TAG, "The connection was broken before it was accepted.");
                            break;
                    }
                }
                @Override
                public void onDisconnected(String endpointId) {
                    //mIsConnected = false;
                    Log.d(TAG, "We're disconnected");
                    txt.append("Disconnect");
                }
            };




    //receive a Payload
    private final PayloadCallback mPayloadCallback =
            new PayloadCallback() {
                @Override
                public void onPayloadReceived(String endpointId, Payload payload) {
                    Log.d(TAG, "Got a Payload from " + endpointId + ", but we should receive something....");
                }
                @Override
                public void onPayloadTransferUpdate(String endpointId, PayloadTransferUpdate update) {
                    if (update.getStatus() == PayloadTransferUpdate.Status.SUCCESS) {
                        Log.d(TAG, "Got a Payload Update from " + endpointId + ", but we should receive something....");
                    }
                }
            };





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //init layout
        setContentView(R.layout.activity_main);
        this.txt = (TextView) findViewById(R.id.txt);
        txt.append("Source: " + sourcePath + "; Destination: " + destinationPath);
        this.btn = (Button) findViewById(R.id.btn);
        btn.setText("unfall");
        btn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                startAccident();
            }
        });

        mConnectionsClient = Nearby.getConnectionsClient(this);








    }

    private void startAccident() {
        txt.append("Unfall ist passiert");
        prepareZip();
        //Eventuell timer einbauen? Zwischen prepareZip() und dem Senden per Nearby muss genug Zeit vergehen um alles zu Zippen.

        startAdvertising();
    }

    private void prepareZip() {
        ZipArchive zipArchive = new ZipArchive();
        zipArchive.zip(sourcePath, destinationPath,"12345");
    }


    /*
    return the zip File as Payload.
     */
    private Payload getZipFile() {
        File file = new File(destinationPath);
        Payload filePayload = null;
        try {
            filePayload = Payload.fromFile(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            txt.setText("FILE NOT FOUND");
        }
        txt.append("File payload should be sended");
        return filePayload;
    }



    /*
   Nearby Methods
    */
    private void startAdvertising() {

        //mIsAdvertising = true;
        mConnectionsClient
                .startAdvertising(
                        /* endpointName= */ "AdvertiserDevice",
                        /* serviceId= */ serviceId,
                        mConnectionLifecycleCallback,
                        new AdvertisingOptions(strategy))
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unusedResult) {
                                Log.d(TAG,"Now Advertising....");
                                txt.append("Advertising is started");
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                //mIsAdvertising = false;
                                Log.d(TAG,"startAdvertising failed e: " +e);
                                txt.append("Advertising failed");
                            }
                        });
    }

    private void stopAdvertising() {
        //mIsAdvertising = false;
        mConnectionsClient.stopAdvertising();
    }

    private void sendData(Payload payload) {
        mConnectionsClient.sendPayload(partnerEndpointName, payload)
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "send Payload failed; e:" + e);
                            }
                        });
    }



}
