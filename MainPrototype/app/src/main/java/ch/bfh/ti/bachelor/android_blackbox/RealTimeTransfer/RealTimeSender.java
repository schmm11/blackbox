package ch.bfh.ti.bachelor.android_blackbox.RealTimeTransfer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.UnsupportedEncodingException;

import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;

public class RealTimeSender {
    private String TAG = "RTT_Sender";
    private Context context;

    //Nearby
    private Strategy strategy = Strategy.P2P_POINT_TO_POINT;
    private String partnerEndpointName;
    private String serviceId = Constants.ID.REAL_TIME_NEARBY_SERVICE_ID;
    private ConnectionsClient mConnectionsClient;

    //broadcastmanager to send gui updates to mainActivity
    private LocalBroadcastManager localBroadcastManager;

    //Receiving Status
    private Boolean isNearbyAdvertising = false;
    private Boolean isNearbyConnected = false;


    /*
    Init Receiver for gui update requests
     */
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == Constants.ACTION.REALTIME_RECEIVER_REQUEST_GUI_UPDATE){
                sendGuiUpdateToMainActivity();
            }
            else{
                Log.wtf(TAG,"Received an impossible Intent Action: "+intent.getAction());
            }
        }




    };



    //Nearby Methods
    //Nearby ConnectionLifeCycle
    private final ConnectionLifecycleCallback mConnectionLifecycleCallback =
            new ConnectionLifecycleCallback() {
                @Override
                public void onConnectionInitiated(String endpointId, ConnectionInfo connectionInfo) {
                    // Automatically accept the connection on both sides.
                    partnerEndpointName = endpointId;
                    Log.d(TAG, "onConnectionInit from" + endpointId);
                    Toast.makeText(context,"RTT: Initiate connection to: " + endpointId.toString(), Toast.LENGTH_SHORT).show();
                    mConnectionsClient.acceptConnection(endpointId, mPayloadCallback);
                }

                @Override
                public void onConnectionResult(String endpointId, ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode()) {
                        case ConnectionsStatusCodes.STATUS_OK:
                            Log.d(TAG, "RTT: We're successfully connected! Ready for real time data transfer.");
                            Toast.makeText(context,"RTT: Ready for transfer! Successfully connected to: " + endpointId.toString(), Toast.LENGTH_SHORT).show();
                            startSendingData();
                            break;
                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            Log.d(TAG, "The connection was rejected by one or both sides");
                            Toast.makeText(context,"RTT ERROR: The connection was rejected by one or both sides! tried to connect to: " + endpointId.toString(), Toast.LENGTH_SHORT).show();
                            stopTransfer();
                            break;
                        default:
                            Log.d(TAG, "The connection was broken before it was accepted.");
                            Toast.makeText(context,"RTT ERROR: The connection was broken before it was accepted.! tried to connect to: " + endpointId.toString(), Toast.LENGTH_SHORT).show();
                            Toast.makeText(context,"RTT ERROR: " + result.toString(), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
                @Override
                public void onDisconnected(String endpointId) {
                    isNearbyConnected = false;
                    isNearbyAdvertising = false;
                    sendGuiUpdateToMainActivity();
                    launchAdvertising();
                    Toast.makeText(context,"RTT: Disconnected from: " + endpointId.toString(), Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "We're disconnected");
                }
            };



    //receive a Payload
    private final PayloadCallback mPayloadCallback =
            new PayloadCallback() {
                @Override
                public void onPayloadReceived(String endpointId, Payload payload) {
                    // Nothing to Do since we are the sender
                }
                @Override
                public void onPayloadTransferUpdate(String endpointId, PayloadTransferUpdate update) {
                    if (update.getStatus() == PayloadTransferUpdate.Status.SUCCESS) {
                        // Nothing to Do since we are the sender
                    }
                }
            };

    public RealTimeSender(Context context){
        //Maybe pass the context another way...
        this.context = context;
        mConnectionsClient = Nearby.getConnectionsClient(context);
        serviceId = PreferenceManager.getDefaultSharedPreferences(context).getString(Constants.SETTINGS.KEY_PREF_TRANSFERID, "myUniqueID");
        localBroadcastManager = LocalBroadcastManager.getInstance(context.getApplicationContext());
        // register local broadcast receiver
        IntentFilter filter = new IntentFilter(Constants.ACTION.REALTIME_RECEIVER_REQUEST_GUI_UPDATE);
        LocalBroadcastManager.getInstance(context).registerReceiver(mReceiver, filter);
    }

    public void launchAdvertising() {
        //only advertise when no
        if(!isNearbyConnected){
            mConnectionsClient
                    .startAdvertising(
                            /* endpointName= */ "AdvertiserDevice",
                            /* serviceId= */ serviceId,
                            mConnectionLifecycleCallback,
                            new AdvertisingOptions(strategy))
                    .addOnSuccessListener(
                            new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void unusedResult) {
                                    Log.d(TAG,"Now Advertising with the service ID: "+ serviceId);
                                    Toast.makeText(context, "RTT: Now Advertising with the service ID: "+ serviceId, Toast.LENGTH_SHORT).show();
                                    isNearbyAdvertising = true;
                                    sendGuiUpdateToMainActivity();
                                }
                            })
                    .addOnFailureListener(
                            new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d(TAG,"startAdvertising failed e: " +e);
                                    Toast.makeText(context, "RTT ERROR: Advertising failed!", Toast.LENGTH_SHORT).show();
                                    isNearbyAdvertising = false;
                                    sendGuiUpdateToMainActivity();
                                }
                            });
        }
        else{
            Log.d(TAG, "Tried to launchAdvertsiing, but we are allreday connected");
            Toast.makeText(context, "RTT ERROR: Tried to launchAdvertsing(), but already connected", Toast.LENGTH_SHORT).show();
        }

    }


    private void startSendingData() {
        isNearbyConnected = true;
        sendGuiUpdateToMainActivity();
    }
    public void stopTransfer() {
        isNearbyConnected = false;
        isNearbyAdvertising = false;
        sendGuiUpdateToMainActivity();
        mConnectionsClient.stopAllEndpoints();

        // unregister local broadcast receiver
        LocalBroadcastManager.getInstance(context).unregisterReceiver(mReceiver);
    }

    public void sendData(String str){
        try {
            Log.v(TAG, "Sending Data");
            mConnectionsClient.sendPayload(partnerEndpointName, Payload.fromBytes(str.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            Log.d(TAG, "We got an UnsupportedEncodingException: "+e);
            Toast.makeText(context, "RTT ERROR: Unsupported encoding exception while sending data!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public void sendGuiUpdateToMainActivity(){
        //Send an Intent to update the Amount GUI in the Activity:
        Intent localIntent = new Intent(Constants.ACTION.REALTIME_RECEIVER_GUI_UPDATE);
        localIntent.putExtra("isNearbyAdvertising", isNearbyAdvertising);
        localIntent.putExtra("isNearbyConnected", isNearbyConnected);
        localBroadcastManager.sendBroadcast(localIntent);
    }
}

