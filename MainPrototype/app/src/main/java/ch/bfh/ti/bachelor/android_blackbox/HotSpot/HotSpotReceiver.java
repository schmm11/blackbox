package ch.bfh.ti.bachelor.android_blackbox.HotSpot;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo;
import com.google.android.gms.nearby.connection.DiscoveryOptions;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import ch.bfh.ti.bachelor.android_blackbox.R;
import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;

public class HotSpotReceiver extends AppCompatActivity {
    private ProgressBar progressBar; //Not needed in the moment.
    private TextView textView;
    private String TAG = "HotspotReceiver";
    //Nearby
    private Strategy strategy = Strategy.P2P_POINT_TO_POINT;
    private String ownEndpointname, partnerEndpointName;
    private String serviceId = Constants.ID.HOTSPOT_NEARBY_SERVICE_ID;
    private ConnectionsClient mConnectionsClient;
    private int amountPayloads = 0; //Just for testing purpose, can be deleted

    //Nearby Connection Lifecycle
    private final ConnectionLifecycleCallback mConnectionLifecycleCallback =
            new ConnectionLifecycleCallback() {
                @Override
                public void onConnectionInitiated(String endpointId, ConnectionInfo connectionInfo) {
                    // Automatically accept the connection
                    partnerEndpointName = endpointId;
                    Log.d(TAG, "onConnectionInit from" + endpointId);
                    mConnectionsClient.acceptConnection(endpointId, mPayloadCallback);
                }

                @Override
                public void onConnectionResult(String endpointId, ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode()) {
                        case ConnectionsStatusCodes.STATUS_OK:
                            //Finished establishing a correct connection
                            Log.d(TAG, "We're sucessfully Nearby connected!");
                            textView.append("Successfully connected to endpoint '" + endpointId.toString() + "'"+System.getProperty("line.separator"));

                            break;
                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            Log.d(TAG, "The connection was rejected by one or both sides");
                            textView.append("Connection to  endpoint '" + endpointId.toString() + "' was rejected"+System.getProperty("line.separator"));
                            break;
                        default:
                            Log.d(TAG, "The connection was broken before it was accepted.");
                            textView.append("Connection to  endpoint '" + endpointId.toString() + "' failed: "+ result.toString() +System.getProperty("line.separator"));
                            break;
                    }
                }
                @Override
                public void onDisconnected(String endpointId) {
                    //mIsConnected = false;
                    Log.d(TAG, "We're disconnected");
                    textView.append("Disconnected from endpoint: " + endpointId + System.getProperty("line.separator"));
                }
            };
    //Nearby Payload Callback
    private final PayloadCallback mPayloadCallback =
            new PayloadCallback() {
                @Override
                public void onPayloadReceived(String endpointId, Payload payload) {
                    //show Progress Bar indeterminate
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                    Toast.makeText(HotSpotReceiver.this,"Receiving data...",Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "Got a Junk Payload from " + endpointId);
                    amountPayloads++;

                }
                @Override
                public void onPayloadTransferUpdate(String endpointId, PayloadTransferUpdate update) {
                    if (update.getStatus() == PayloadTransferUpdate.Status.SUCCESS) {
                        textView.append("Finish! The remote data has been successfully transferred (amount of Payloads: "+ amountPayloads +")");
                        textView.append("You can find the file in the directory 'Downloads/Nearby'."+System.getProperty("line.separator"));
                        textView.append("Rename the file to make sure the extension is '.zip'!"+System.getProperty("line.separator"));
                        textView.append("Disconnecting endpoints..."+System.getProperty("line.separator"));
                        Log.d(TAG, "payload transmitted");
                        //Stop the Progress Bar
                        progressBar.setVisibility(ProgressBar.INVISIBLE);

                        mConnectionsClient.stopAllEndpoints();

                    }
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hot_spot_receiver);

        textView = findViewById(R.id.txt_Status);
        progressBar = findViewById(R.id.progressBar);
        mConnectionsClient = Nearby.getConnectionsClient(this);
        textView.setText("Hotspot Receiver started."+ System.getProperty("line.separator") +"Searching for hotspots with the service id '"+serviceId+ "'..." + System.getProperty("line.separator"));
        startDiscovering();
    }


    private void startDiscovering() {
        mConnectionsClient
                .startDiscovery(
                        serviceId,
                        new EndpointDiscoveryCallback() {
                            @Override
                            public void onEndpointFound(String endpointId, DiscoveredEndpointInfo info) {
                                Log.d(TAG, "Endpoint Found, Name is: "+ endpointId);
                                textView.append("Endpoint Found: " + endpointId + System.getProperty("line.separator"));
                                if (serviceId.equals(info.getServiceId())) {
                                    partnerEndpointName = endpointId;

                                    // Ask for a Connection
                                    mConnectionsClient
                                            .requestConnection("DiscovererName", partnerEndpointName, mConnectionLifecycleCallback)
                                            .addOnFailureListener(
                                                    new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            Log.d(TAG, "onEndpointDiscovered failed");
                                                            textView.append("Failed to connect to discovered endpoint" + System.getProperty("line.separator"));
                                                        }
                                                    });
                                }
                            }
                            @Override
                            public void onEndpointLost(String endpointId) {
                                Log.d(TAG, "onEndpointLost failed");

                            }
                        },
                        new DiscoveryOptions(strategy))
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unusedResult) {
                                textView.append(" Discovery started...."+System.getProperty("line.separator"));

                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "Discovery failed");
                                textView.append(" Discovery failed...."+System.getProperty("line.separator"));
                            }
                        });
    }
}
