package ch.bfh.ti.bachelor.android_blackbox.RealTimeTransfer;

import android.app.ActivityManager;
import android.arch.lifecycle.LifecycleOwner;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.nearby.Nearby;

import ch.bfh.ti.bachelor.android_blackbox.MainActivity;
import ch.bfh.ti.bachelor.android_blackbox.R;
import ch.bfh.ti.bachelor.android_blackbox.RecordingService;
import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;
/*
This Activity is only for the Real Time Transfer
It Starts a ForegroundService
 */
public class RealTimeTransferReceiverActivity extends AppCompatActivity {

    //GUI
    private TextView textAmount;
    private String TAG = "RTTReceiver";
    private Button btn_StartReceiver, btn_StopReceiver;



    //Broadcast for Nearby Amount Updates
    /*
    Init Receiver for local Broadcasts
     */
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //Received a Realtime Intent

            if (intent.getAction() == Constants.ACTION.REALTIME_RECEIVER_GUI_UPDATE){
                refreshAmounTextview(intent.getStringExtra("amount"));
                Log.d(TAG, "Intent:" + intent.getAction());
            }
            else{
                Log.wtf(TAG,"Received an impossible Intent Action: "+intent.getAction());
            }
        }




    };

    private void refreshAmounTextview(String amount) {
        textAmount.setText(amount);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_time_transfer_receiver);
        textAmount = findViewById(R.id.txt_Amount);

        this.btn_StartReceiver = findViewById(R.id.btn_StartReceiver);
        btn_StartReceiver.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "on Start Click => Startin RelTimeTransferReceiverService");

                //start service
                Intent startIntent = new Intent(RealTimeTransferReceiverActivity.this, RealTimeTransferReceiverService.class);
                startIntent.setAction(Constants.ACTION.REALTIME_RECEIVER_ACTION_START);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(startIntent);
                } else {
                    startService(startIntent);
                }
            }
        });

        this.btn_StopReceiver = findViewById(R.id.btn_StopReceiver);
        btn_StopReceiver.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "on Start Click => Stoping RelTimeTransferReceiverService");

                //start service
                Intent startIntent = new Intent(RealTimeTransferReceiverActivity.this, RealTimeTransferReceiverService.class);
                startIntent.setAction(Constants.ACTION.REALTIME_RECEIVER_ACTION_STOP);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(startIntent);
                } else {
                    startService(startIntent);
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(Constants.ACTION.REALTIME_RECEIVER_GUI_UPDATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter);
    }

}