package ch.bfh.ti.bachelor.android_blackbox.Sensor;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import ch.bfh.ti.bachelor.android_blackbox.Saver.AccelerationSaver;
import ch.bfh.ti.bachelor.android_blackbox.Saver.LinearAccelerationSaver;
import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;

public class LinearAccelerationSensorListener implements SensorEventListener, SensorInterface {
    private Sensor sensor;
    private LinearAccelerationSaver saver = new LinearAccelerationSaver();
    private static final String TAG = "Blackbox_AccListener";
    private SensorManager mSensorManager;
    private String strToSave ="";
    private long amount;
    private Boolean realTimeEnabled;
    private String intentAction = Constants.ACTION.REALTIME_TRANSFER_SEND;
    private LocalBroadcastManager localBroadcastManager;


    public LinearAccelerationSensorListener(Context context, Boolean realTimeEnabled){
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        this.realTimeEnabled = realTimeEnabled;
        if(realTimeEnabled){
            localBroadcastManager = LocalBroadcastManager.getInstance(context);
        }

    }



    @Override
    public void onSensorChanged(SensorEvent event) {
        Log.v(TAG, "Linear Acceleration SensorData received");
        long timestamp = System.currentTimeMillis();
        this.strToSave += timestamp + ";" + event.timestamp + ";" + event.values[0] + ";" + event.values[1] + ";" + event.values[2] + System.getProperty ("line.separator");
        amount++;

        /*
        Only call save() every n time => sonst verhacken sich die Save Befehle
         */
        if(amount % 6 == 0) {
            saver.save(strToSave);
            strToSave ="";
            Log.v(TAG, "onSensorChangedisCalled amount: " + amount);
        }

        /*
        When RealTimeTransfer is enabled: Send it
         */
        if(realTimeEnabled){
            // Send Data here
            Intent localIntent = new Intent(intentAction);
            localIntent.putExtra("data", "lac;" + strToSave);
            localBroadcastManager.sendBroadcast(localIntent);

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void start() {
        mSensorManager.registerListener((SensorEventListener) this, sensor, SensorManager.SENSOR_DELAY_GAME);
        saver.save("Timestamp [ms]; Event-Timestamp [ns]; X-axis [m/s^2]; Y-axis [m/s^2]; Z-axis [m/s^2]" + System.getProperty ("line.separator")); //headlines
    }

    @Override
    public void stop() {
        mSensorManager.unregisterListener(this, sensor);
    }

    @Override
    public void restart() {
        saver = new LinearAccelerationSaver();
        stop();
        start();
    }
}
