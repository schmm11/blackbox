package ch.bfh.ti.bachelor.android_blackbox.Sensor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import java.util.Collections;

import ch.bfh.ti.bachelor.android_blackbox.RecordingService;
import ch.bfh.ti.bachelor.android_blackbox.Saver.CrashSaver;
import ch.bfh.ti.bachelor.android_blackbox.Settings.SettingsActivity;
import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;

public class CrashDetectorListener implements SensorEventListener, SensorInterface {
    private Sensor sensorPressure, sensorAcceleration, sensorGyroscope;
    private CrashSaver crashSaver = new CrashSaver();
    private static final String TAG = "BlackBox_CrashDetection";
    private SensorManager mSensorManager;
    private long totalCrashAmount = 0;
    private final Context context;

    private Double actualGForce = 0.0;    // values from acceleration sensor are double
    private Double gForceAlertValue;      // get later from settings

    private Float actualPressure = 0f;    // values from barometer sensor are in float
    private Float previousPressure = 0f;
    private Float pressureAlertValue;     // get later from settings

    private Float gyroscopeAlertValue = 0f;


    private SharedPreferences sharedPref;



    public CrashDetectorListener(Context context) {
        this.context = context;
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        //set alert values:
        gForceAlertValue = Double.valueOf(sharedPref.getString(Constants.SETTINGS.KEY_PREF_GFORCETHRESHOLD, "3.0"));
        pressureAlertValue = Float.valueOf(sharedPref.getString(Constants.SETTINGS.KEY_PREF_AIRBAGTHRESHOLD, "3.0"));
        gyroscopeAlertValue = Float.valueOf(sharedPref.getString(Constants.SETTINGS.KEY_PREF_GYROSCOPETHRESHOLD, "3.14"));

        }

    @Override
    public void onSensorChanged(SensorEvent event) {


    // Beschleunigung
        if(event.sensor.getType()== Sensor.TYPE_LINEAR_ACCELERATION) {

            // Calculate directionless GForce: math.sqrt (x^2 + y^2 + z^2 )/9.8
            actualGForce = Math.sqrt(event.values[0] * event.values[0] + event.values[1] * event.values[1] + event.values[2] * event.values[2]) / 9.81;

            //When delta is bigger than x => Alarm
            if (actualGForce > gForceAlertValue ) {
                Log.d(TAG, "Alarm: Beschleunigung Delta ist: " + String.format("%.2f", actualGForce) + "Gesamtanzahl Crashes: " + totalCrashAmount);
                totalCrashAmount++;
                String reason = "Accelerometer delta: " + String.format("%.2f", actualGForce);
                sendAlarm(reason);
                crashSaver.save(System.currentTimeMillis() + ";" + reason + ";" + event.values[0] + ";y:" + event.values[1] + ";z:" + event.values[2] + System.getProperty ("line.separator"));
            }

        }
        // Luftdruck
        if(event.sensor.getType()== Sensor.TYPE_PRESSURE) {
            actualPressure = event.values[0];
            float deltaPressure = 0;
            if (previousPressure != 0) {
                deltaPressure = actualPressure - previousPressure;
            }
            if (Math.abs(deltaPressure) > pressureAlertValue) {
                Log.d(TAG, "Alarm. Lufdruck Delta "+ deltaPressure+ " Gesamtzahl Crashes:");
                totalCrashAmount++;
                String reason = "Barometer delta: " + deltaPressure;
                sendAlarm(reason);
                crashSaver.save(System.currentTimeMillis() + ";" + reason + ";Pressure:"+ actualPressure + System.getProperty ("line.separator"));
            }
            previousPressure = actualPressure;
        }

        // Gyroscope
        if(event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            if( Math.abs(event.values[0]) > gyroscopeAlertValue || Math.abs(event.values[1]) > gyroscopeAlertValue || Math.abs(event.values[2]) > gyroscopeAlertValue) {
                String reason = "unknown Gyroscope";
                if (Math.abs(event.values[0]) > gyroscopeAlertValue) {
                    reason = "Gyroscope X-axis (Roll) delta: " + Float.toString(event.values[0]);
                } else if (Math.abs(event.values[1]) > gyroscopeAlertValue) {
                    reason = "Gyroscope Y-axis (Pitch) delta: " + Float.toString(event.values[1]);
                } else if (Math.abs(event.values[2]) > gyroscopeAlertValue) {
                    reason = "Gyroscope Z-axis (Yaw) delta: " + Float.toString(event.values[2]);
                }

                Log.d(TAG, "Alarm. Gyroscope ist  zu hoch");
                totalCrashAmount++;
                sendAlarm(reason);
                crashSaver.save(System.currentTimeMillis() + " ; " + reason + ";" + Float.toString(event.values[0]) + ";" + Float.toString(event.values[1]) + ";" + Float.toString(event.values[2]) + System.getProperty("line.separator"));
            }
        }
}

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void start() {
        sensorPressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        mSensorManager.registerListener((SensorEventListener) this, sensorPressure, SensorManager.SENSOR_DELAY_GAME);

        sensorAcceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mSensorManager.registerListener((SensorEventListener) this, sensorAcceleration, SensorManager.SENSOR_DELAY_GAME);

        sensorGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mSensorManager.registerListener((SensorEventListener) this, sensorGyroscope, SensorManager.SENSOR_DELAY_GAME);

        crashSaver.save("Timestamp [ms]; Reason; Data; Data; Data"+ System.getProperty ("line.separator")); //headlines

    }
    @Override
    public void stop() {
        mSensorManager.unregisterListener(this, sensorPressure);
        mSensorManager.unregisterListener(this, sensorAcceleration);
        mSensorManager.unregisterListener(this, sensorGyroscope);
    }

    @Override
    public void restart() {
        // Do not restart this class! => we do nothing
    }


    /*
    This method is called when a sensor exceed the threshold
     */
    private void sendAlarm(String reason){
        // alarm the Main Activity
        Intent alarmIntent = new Intent(context, RecordingService.class);
        alarmIntent.setAction(Constants.ACTION.ALARM_ACTION);
        alarmIntent.putExtra("reason", reason);
        context.startService(alarmIntent);
        Log.d(TAG, "sending alarm intent");
    }

}
