package ch.bfh.ti.bachelor.android_blackbox.HotSpot;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;

import ch.bfh.ti.bachelor.android_blackbox.Settings.SettingsActivity;
import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;
import ir.mahdi.mzip.zip.ZipArchive;


public class HotSpotSender {
    private String TAG = "EmergencySender";
    private Context context;

    //Needed for the password
    private SharedPreferences sharedPref;
    private String password;

    //File
    private String sourcePath = Environment.getExternalStorageDirectory() + Constants.PATHS.RECORD_SAVE_PATH + "/";
    private String destinationPath;

    //Nearby
    private Strategy strategy = Strategy.P2P_POINT_TO_POINT;
    private String partnerEndpointName;
    private String serviceId = Constants.ID.HOTSPOT_NEARBY_SERVICE_ID;
    private ConnectionsClient mConnectionsClient;

    //Nearby ConnectionLifeCycle
    private final ConnectionLifecycleCallback mConnectionLifecycleCallback =
            new ConnectionLifecycleCallback() {
                @Override
                public void onConnectionInitiated(String endpointId, ConnectionInfo connectionInfo) {
                    // Automatically accept the connection on both sides.
                    partnerEndpointName = endpointId;
                    Log.d(TAG, "onConnectionInit from" + endpointId);
                    mConnectionsClient.acceptConnection(endpointId, mPayloadCallback);
                }

                @Override
                public void onConnectionResult(String endpointId, ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode()) {
                        case ConnectionsStatusCodes.STATUS_OK:
                            Log.d(TAG, "We're sucessfully Nearby connected!");
                            Toast.makeText(context, "Nearby Hotspot: Connected to a receiver", Toast.LENGTH_LONG).show();
                            startSendingData();
                            break;
                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            Log.d(TAG, "The connection was rejected by one or both sides");
                            break;
                        default:
                            Log.d(TAG, "The connection was broken before it was accepted.");
                            break;
                    }
                }
                @Override
                public void onDisconnected(String endpointId) {
                    Log.d(TAG, "We're disconnected");
                }
            };

    //receive a Payload
    private final PayloadCallback mPayloadCallback =
            new PayloadCallback() {
                @Override
                public void onPayloadReceived(String endpointId, Payload payload) {
                    //Log.d(TAG, "Got a Payload from " + endpointId + ", but we should receive something....");
                }
                @Override
                public void onPayloadTransferUpdate(String endpointId, PayloadTransferUpdate update) {
                    if (update.getStatus() == PayloadTransferUpdate.Status.SUCCESS) {
                        Log.d(TAG, "PayloadTransferUpdate success => Finished");
                        Toast.makeText(context, "Nearby Hotspot: Finished transfer", Toast.LENGTH_LONG).show();

                        deactivateHotSpot();
                    }
                }
            };


    public HotSpotSender(Context context){
        //Maybe pass the context another way...
        this.context = context;
        mConnectionsClient = Nearby.getConnectionsClient(context);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        password = sharedPref.getString(Constants.SETTINGS.KEY_PREF_HOTSPOTPASSWORD,"12345");

        }

    /*
    This method is called from outside to start an hotspot
     */
    public void activateHotspot(){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HH-mm-ss");
        destinationPath = Environment.getExternalStorageDirectory()+ Constants.PATHS.HOTSPOT_ZIP_PATH + "AndroidBlackboxBackup_" + sdf.format(System.currentTimeMillis()) + ".zip";

        Log.d(TAG, "activateHotspot()");
        //First: Zip what we want
        //asynctasask start here
        new ZipTask().execute("nonsense");
    }


    /*
    This method is called when an sucessfull Nearby connection is made
     */
    private void startSendingData(){
        mConnectionsClient.sendPayload(partnerEndpointName, getZipFile());
    }




    private void startAdvertising() {
        mConnectionsClient
                .startAdvertising(
                        /* endpointName= */ "AdvertiserDevice",
                        /* serviceId= */ serviceId,
                        mConnectionLifecycleCallback,
                        new AdvertisingOptions(strategy))
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unusedResult) {
                                Log.d(TAG,"Now Advertising....");
                                Toast.makeText(context, "Nearby Hotspot: now Advertising", Toast.LENGTH_LONG).show();
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG,"startAdvertising failed error: " +e);
                            }
                        });
    }

    /*
    return the zip from the destination Path as Payload.
     */
    private Payload getZipFile() {
        File file = new File(destinationPath);
        Payload filePayload = null;
        try {
            filePayload = Payload.fromFile(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filePayload;
    }


    public void deactivateHotSpot() {
        mConnectionsClient.stopAllEndpoints();
    }


    private class ZipTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            /* Powered by MZIP
            See Dependencies under build.gradle for more Infos.
             */
            ZipArchive zipArchive = new ZipArchive();
            zipArchive.zip(sourcePath, destinationPath ,password);

            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "End zip: PostExecute;");
            //Start Sending Nearby Mehtod here
            Toast.makeText(context, "Nearby Hotspot: finished Zipping", Toast.LENGTH_LONG).show();
            startAdvertising();
        }

        @Override
        protected void onPreExecute() {
            Log.d(TAG, "Start zip: PreExecute();");
            Toast.makeText(context, "Nearby Hotspot: Start zipping data.. this may take a while!", Toast.LENGTH_LONG).show();

        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}

