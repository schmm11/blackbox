package ch.bfh.ti.bachelor.android_blackbox.Sensor;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import ch.bfh.ti.bachelor.android_blackbox.Saver.BarometerSaver;
import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;

public class BarometerSensorListener implements SensorEventListener, SensorInterface {
    private Sensor sensor;
    private BarometerSaver saver = new BarometerSaver();
    private static final String TAG = "Blackbox_BarListener";
    private SensorManager mSensorManager;
    private String strToSave = "";
    private long amount;
    private Boolean realTimeEnabled;
    private String intentAction = Constants.ACTION.REALTIME_TRANSFER_SEND;
    private LocalBroadcastManager localBroadcastManager;


    public BarometerSensorListener(Context context,Boolean realTimeEnabled){
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        this.realTimeEnabled = realTimeEnabled;

        if(realTimeEnabled){
            localBroadcastManager = LocalBroadcastManager.getInstance(context);
        }

    }



    @Override
    public void onSensorChanged(SensorEvent event) {
        Log.v(TAG, "Barometer SensorData received");
        long timestamp = System.currentTimeMillis();

        this.strToSave += timestamp + ";" + event.timestamp + ";" + event.values[0] +  System.getProperty ("line.separator");


        amount++;

        /*
        Only call save() every n time => sonst verhacken sich die Save Befehle
         */
        if(amount % 6 == 0) {
            saver.save(strToSave);
            strToSave ="";
            Log.v(TAG, "onSensorChangedisCalled amount: " + amount);
        }

        /*
        When RealTimeTransfer is enabled: Send it
         */
        if(realTimeEnabled){
            // Send Data here
            Intent localIntent = new Intent(intentAction);
            localIntent.putExtra("data", "bar;" + strToSave);
            localBroadcastManager.sendBroadcast(localIntent);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void start() {
        mSensorManager.registerListener((SensorEventListener) this, sensor, SensorManager.SENSOR_DELAY_GAME);
        saver.save("Timestamp [ms]; Event-Timestamp [ns]; Pressure [mbar]" + System.getProperty ("line.separator")); //headlines
    }

    @Override
    public void stop() {
        mSensorManager.unregisterListener(this, sensor);
    }

    @Override
    public void restart() {
        saver = new BarometerSaver();
        stop();
        start();
    }
}
