package ch.bfh.ti.bachelor.android_blackbox.Sensor;

public interface SensorInterface {

public void start();

public void stop();

public void restart();

}
