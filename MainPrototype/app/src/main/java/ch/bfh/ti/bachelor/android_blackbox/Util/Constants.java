package ch.bfh.ti.bachelor.android_blackbox.Util;

public class Constants {

    public interface STATICS {
        Integer ALARM_TRESHOLD_IN_MS = 15000;
    }

    public interface SETTINGS {
        String KEY_PREF_ACCELERATION = "pref_acceleration";
        String KEY_PREF_GPS = "pref_gps";
        String KEY_PREF_BAROMETER = "pref_barometer";
        String KEY_PREF_GYRO = "pref_gyro";
        String KEY_PREF_COMPASS = "pref_compass";
        String KEY_PREF_VIDEO = "pref_video";
        String KEY_PREF_AUDIO = "pref_audio";
        String KEY_PREF_CAMERA = "pref_camera";
        String KEY_PREF_RESOLUTION = "pref_resolution";


        String KEY_PREF_LOOPRECORDING = "pref_loopRecording";
        String KEY_PREF_LOOPLENGTH = "pref_loopLength";
        String KEY_PREF_PARTLENGTH =  "pref_partLength";

        String KEY_PREF_HOTSPOT = "pref_hotspot";
        String KEY_PREF_HOTSPOTPASSWORD = "pref_hotspotPassword";

        String KEY_PREF_CRASHRECOGNITION = "pref_crashRecognition";
        String KEY_PREF_GFORCETHRESHOLD = "pref_gForceThreshold";
        String KEY_PREF_AIRBAGTHRESHOLD = "pref_airbagThreshold";
        String KEY_PREF_GYROSCOPETHRESHOLD = "pref_gyroscopeThreshold";

        String KEY_PREF_AUTOSTART = "pref_autoStart";
        String KEY_PREF_DARKTHEME = "pref_darktheme";

        String KEY_PREF_REALTIMETRANSFER = "pref_realtimeTransfer";
        String KEY_PREF_TRANSFERID = "pref_realtimeTransferID";
    }



    public interface ACTION {
        String START_RECORDING_ACTION = "ch.bfh.ti.bachelor.android_blackbox.action.startrecording";
        String STOP_RECORDING_ACTION = "ch.bfh.ti.bachelor.android_blackbox.action.stoprecording";
        String STOP_RECORDING_CLEAR_LOOP = "ch.bfh.ti.bachelor.android_blackbox.action.stoprecordingandclear";
        String CRASH_SAVE_DATA_ACTION = "ch.bfh.ti.bachelor.android_blackbox.action.crash";
        String CRASH_NO_REACTION_ACTION = "ch.bfh.ti.bachelor.android_blackbox.action.crashNoReaction";
        String CRASH_FALSE_POSITIVE_ACTION = "ch.bfh.ti.bachelor.android_blackbox.action.crashFalsePositive";
        String ALARM_ACTION = "ch.bfh.ti.bachelor.android_blackbox.action.alarm";
        String ALARM_ACTION_NOTIFICATION = "ch.bfh.ti.bachelor.android_blackbox.action.alarm.notification";
        String REALTIME_TRANSFER_SEND="ch.bfh.ti.bachelor.android_blackbox.action.transferSend";

        //RealTimeTransferReceiver
        String REALTIME_RECEIVER_ACTION_START = "ch.bfh.ti.bachelor.android_blackbox.action.startreceiver";
        String REALTIME_RECEIVER_ACTION_STOP = "ch.bfh.ti.bachelor.android_blackbox.action.stopreceiver";
        String REALTIME_RECEIVER_GUI_UPDATE = "ch.bfh.ti.bachelor.android_blackbox.action.guiupdate";
        String REALTIME_RECEIVER_REQUEST_GUI_UPDATE = "ch.bfh.ti.bachelor.android_blackbox.action.requestguiupdate";
    }

    public interface PATHS {
        String DATA_ROOT_PATH = "/Documents/AndroidBlackbox";
        String RECORD_SAVE_PATH =  DATA_ROOT_PATH + "/local";
        String RECORD_ALARM_PATH = DATA_ROOT_PATH + "/local/Alarm";
        String HOTSPOT_ZIP_PATH = DATA_ROOT_PATH + "/zip";
        //String HOTSPOT_BACKUP_DOWNLOAD_PATH = "/Downloads/Nearby";
        String REALTIME_TRANSFER_DATA_PATH = DATA_ROOT_PATH + "/remote";
    }

    public interface ID {
        int RECORDING_SERVICE_ID = 101;
        int REAL_TIME_TRANSFER_RECEIVER_ID = 102;
        String RECORDING_NOTIFICATION_CHANNEL_ID = "Android Blackbox Recording Service";
        String REAL_TIME_TRANSFER_RECEIVER_NOTIFICATION_CHANNEL_ID = "Android Blackbox RTT Service";
        String HOTSPOT_NEARBY_SERVICE_ID = "BlackboxEmergency";
        String REAL_TIME_NEARBY_SERVICE_ID = "BlackboxRealTimeTransfer";
    }
}
