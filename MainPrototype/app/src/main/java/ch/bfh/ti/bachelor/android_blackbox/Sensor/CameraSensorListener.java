package ch.bfh.ti.bachelor.android_blackbox.Sensor;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;

public class CameraSensorListener extends ContextWrapper implements SensorInterface {
    public final String TAG = "CameraSensorListener";
    private SurfaceView mSurfaceView;
    private SurfaceHolder mSurfaceHolder;
    private static Camera mServiceCamera;
    private boolean mRecordingStatus;
    private MediaRecorder mMediaRecorder;
    private SharedPreferences sharedPref;
    private  Camera.Parameters cameraParameters;

    public CameraSensorListener(Context base, SurfaceHolder surfaceHolder, SurfaceView surfaceView) {
        super(base);
        mSurfaceView = surfaceView;       //MainActivity.mSurfaceView; //do we need this?
        mSurfaceHolder = surfaceHolder;   //MainActivity.mSurfaceHolder;
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        mRecordingStatus = false;
    }


    @Override
    public void start() {
        if(!mRecordingStatus){
            startRecording();
        }
    }

    @Override
    public void stop() {
        if(mRecordingStatus){
            stopRecording();
        }
    }

    @Override
    public void restart() {
        // not needed in Camera makes itself
    }

    /*
------------------------------------------------------------------------------------------------

                startRecording
                starts the video recording

------------------------------------------------------------------------------------------------
 */
    private boolean startRecording(){
        try {
            if (prepareCamera()){
                if(setPreview()){
                    mServiceCamera.unlock();
                    mMediaRecorder = new MediaRecorder();
                    if(configureMediaRecorder()){
                        mMediaRecorder.prepare();
                        mMediaRecorder.start();
                        mRecordingStatus = true;
                    }
                }
            }
            Toast.makeText(getBaseContext(), String.format("Selected Size (%d x %d)", cameraParameters.getPreviewSize().width, cameraParameters.getPreviewSize().height), Toast.LENGTH_SHORT).show();

            return true;

        } catch (IllegalStateException e) {
            Log.e(TAG, "illegalStateException in CameraSensorListener");
            e.printStackTrace();
            return false;

        } catch (IOException e) {
            Log.e(TAG, "ioException in CameraSensorListener");
            e.printStackTrace();
            return false;
        }
    }


    /*
    ------------------------------------------------------------------------------------------------

                    prepare Camera
                    prepares the selected camera with the selected resolution to record

    ------------------------------------------------------------------------------------------------
     */
    private boolean prepareCamera(){
        try {
            String camID = sharedPref.getString(Constants.SETTINGS.KEY_PREF_CAMERA, "1");
            String resolution = sharedPref.getString(Constants.SETTINGS.KEY_PREF_RESOLUTION, "720");
            String resolutionWidth = resolution.substring(0, resolution.indexOf("x"));                                        // stored in the Preference like "1920x1080"
            String resolutionHeight = resolution.substring(resolution.indexOf("x") + 1, resolution.length());

            mServiceCamera = Camera.open(Integer.valueOf(camID));

            cameraParameters = mServiceCamera.getParameters();

            final List<Camera.Size> listPreviewSize = cameraParameters.getSupportedPreviewSizes();
            for (Camera.Size size : listPreviewSize) {

                if (size.height == Integer.valueOf(resolutionHeight) & size.width == Integer.valueOf(resolutionWidth)) {
                    cameraParameters.setPreviewSize(size.width, size.height);
                }
                Log.d(TAG, String.format("Supported Preview Size (%d, %d)", size.width, size.height));
            }
            cameraParameters.setRecordingHint(true);
            mServiceCamera.setParameters(cameraParameters);
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
            return false;
        }
        return true;
    }

    /*
------------------------------------------------------------------------------------------------

                setPreview
                attaches the camera preview to the surfaceHolder in MainActivity

------------------------------------------------------------------------------------------------
 */
    private boolean setPreview(){
        try {
            mServiceCamera.setPreviewDisplay(mSurfaceHolder);
            mServiceCamera.startPreview();
        }
        catch (IOException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /*
------------------------------------------------------------------------------------------------

                configureMediaRecorder
                prepares the mediaRecorder to record video (the order is very important!)
                    - select codecs, bitrate and container for video and audio

------------------------------------------------------------------------------------------------
 */
    private boolean configureMediaRecorder(){
        try{
            mMediaRecorder.setCamera(mServiceCamera);
            //if audio is enabled
            if(sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_AUDIO,true)){
                mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
            }
            mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
            //if audio is enabled
            if(sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_AUDIO,true)){
                mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            }
            mMediaRecorder.setVideoEncodingBitRate(10000000);
            mMediaRecorder.setVideoSize(cameraParameters.getPreviewSize().width, cameraParameters.getPreviewSize().height);
            mMediaRecorder.setOutputFile(getVideoFilePath().getPath());
            mMediaRecorder.setPreviewDisplay(mSurfaceHolder.getSurface());

            if(sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_LOOPRECORDING,false)){
                int partLengthSeconds = Integer.valueOf(sharedPref.getString(Constants.SETTINGS.KEY_PREF_PARTLENGTH,"30"));
                mMediaRecorder.setMaxDuration(partLengthSeconds*1000);
                mMediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
                    @Override
                    public void onInfo(MediaRecorder mr, int what, int extra) {
                        if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                            restartVideoRecording();
                        }
                    }
                });
            }

        }catch (Exception e){
            Log.e(TAG,e.getMessage());
            return false;
        }
        return true;
    }


    /*
------------------------------------------------------------------------------------------------

                restartVideoRecording
                reset only the necessary things to restart the recording as fast as possible

------------------------------------------------------------------------------------------------
 */
    private void restartVideoRecording() {
        mMediaRecorder.stop();
        mMediaRecorder.reset();

        if (setPreview()) {
            mServiceCamera.unlock();
            mMediaRecorder = new MediaRecorder();
            if (configureMediaRecorder())
            {
                try {
                    mMediaRecorder.prepare();
                    mMediaRecorder.start();
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        }
    }

    /*
------------------------------------------------------------------------------------------------

                stopRecording
                stops the recording and reset the mediaRecorder

------------------------------------------------------------------------------------------------
 */
    private void stopRecording() {
        try {
            mServiceCamera.reconnect();

        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mMediaRecorder.stop();
            mMediaRecorder.reset();

            mServiceCamera.stopPreview();
            mMediaRecorder.release();

            mServiceCamera.release();
            mServiceCamera = null;
        }catch (Exception e){
            Log.e(TAG,"exception in stoprecording() of CameraSensorListener");
        }
    }


    /*
------------------------------------------------------------------------------------------------

                getVideoFilePath
                returns the filepath to save the video file

------------------------------------------------------------------------------------------------
 */
    private File getVideoFilePath() {

        final File filepath = new File(Environment.getExternalStorageDirectory() + Constants.PATHS  .RECORD_SAVE_PATH);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HH-mm-ss");


        if (!filepath.exists()) {
            filepath.mkdirs();
        }
        File file = new File(filepath.getPath(), sdf.format(System.currentTimeMillis()) + "_Video.mp4");

        return file;
    }
}
