package ch.bfh.ti.bachelor.android_blackbox;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;
import java.util.ArrayList;

import ch.bfh.ti.bachelor.android_blackbox.Dialog.AlarmDialog;
import ch.bfh.ti.bachelor.android_blackbox.Dialog.ArrivedSafelyDialog;
import ch.bfh.ti.bachelor.android_blackbox.HotSpot.HotSpotSender;
import ch.bfh.ti.bachelor.android_blackbox.RealTimeTransfer.RealTimeSender;
import ch.bfh.ti.bachelor.android_blackbox.Sensor.AccelerationSensorListener;
import ch.bfh.ti.bachelor.android_blackbox.Sensor.BarometerSensorListener;
import ch.bfh.ti.bachelor.android_blackbox.Sensor.CameraSensorListener;
import ch.bfh.ti.bachelor.android_blackbox.Sensor.CompassSensorListener;
import ch.bfh.ti.bachelor.android_blackbox.Sensor.CrashDetectorListener;
import ch.bfh.ti.bachelor.android_blackbox.Sensor.GPSSensorListener;
import ch.bfh.ti.bachelor.android_blackbox.Sensor.GyroscopeSensorListener;
import ch.bfh.ti.bachelor.android_blackbox.Sensor.LinearAccelerationSensorListener;
import ch.bfh.ti.bachelor.android_blackbox.Sensor.SensorInterface;
import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;
import ch.bfh.ti.bachelor.android_blackbox.Util.LoopCycleDeleter;

public class RecordingService extends Service {
    private static final String TAG = "RecordingService";
    private SharedPreferences sharedPref;
    private ArrayList<SensorInterface> sensorList = new ArrayList<>();
    private static Handler sensorRecordRestartHandler = new Handler();
    private LoopCycleDeleter loopCycleDeleter;
    public boolean isRecording;
    private boolean isInAlarmMode = false;
    private HotSpotSender mHotSpotSender;
    private boolean realTimeTransferEnabled;
    private RealTimeSender realTimeSender;
    private String IntentFilterString_RTTransfer = Constants.ACTION.REALTIME_TRANSFER_SEND;

    /*
    Init Receiver for local Broadcasts
    Receives the RealTime Data from the sensors and sends them via Nearby
     */
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //Received a real time Intent
            if (intent.getAction() == IntentFilterString_RTTransfer){
                realTimeSender.sendData(intent.getStringExtra("data"));
            }
            else{
                Log.wtf(TAG,"Received an impossible Intent Action: "+intent.getAction());
            }
        }




    };

    @Override
    public void onCreate() {
        super.onCreate();

        // Settings:
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        isRecording = false;
        realTimeTransferEnabled = sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_REALTIMETRANSFER, false);

        // Nearby: Start to advertise.
        if (realTimeTransferEnabled){
            realTimeSender = new RealTimeSender(this);
            realTimeSender.launchAdvertising();
            //initNearby();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {

            if (intent.getAction().equals(Constants.ACTION.START_RECORDING_ACTION)) {
                Toast.makeText(this, "start recording...", Toast.LENGTH_SHORT).show();
                reattachRecordingNotification();
                startRecording(MainActivity.mSurfaceHolder, MainActivity.mSurfaceView);

                //Register the local broadcast receiver
                if(realTimeTransferEnabled) {
                    IntentFilter filter = new IntentFilter(Constants.ACTION.REALTIME_TRANSFER_SEND);
                    LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter);
                }

            } else if (intent.getAction().equals(Constants.ACTION.STOP_RECORDING_ACTION)) {

                Toast.makeText(this, "stop recording pressed...", Toast.LENGTH_SHORT).show();

                if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_LOOPRECORDING, true)) {
                        Intent arrivedSafelyDialog = new Intent(RecordingService.this, ArrivedSafelyDialog.class);
                        arrivedSafelyDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(arrivedSafelyDialog);
                } else {
                    stopRecording(false);
                }

            } else if (intent.getAction().equals(Constants.ACTION.STOP_RECORDING_CLEAR_LOOP)) {
                    stopRecording(true); //in case the recording is active..
            } else if (intent.getAction().equals(Constants.ACTION.ALARM_ACTION)) {

                String reasonData = intent.getExtras().getString("reason");
                launchAlarm(reasonData);
            //Since the Notification Intent is a pendingIntent, it does NOT support an easy way to put extras => separate Alarm Action for the notification
            } else if (intent.getAction().equals(Constants.ACTION.ALARM_ACTION_NOTIFICATION) ) {
                String reasonData = "Manually pressed the Alarm Button in the Notification Area";
                launchAlarm(reasonData);

            } else if (intent.getAction().equals(Constants.ACTION.CRASH_SAVE_DATA_ACTION)) {
                Toast.makeText(this, "alarm button pressed!", Toast.LENGTH_SHORT).show();
                crashSaveData();

            } else if (intent.getAction().equals(Constants.ACTION.CRASH_NO_REACTION_ACTION)) {
                crashNoReaction();

            } else if (intent.getAction().equals(Constants.ACTION.CRASH_FALSE_POSITIVE_ACTION)) {
                crashFalsePositive();
            }
        }
        return START_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }





    public void startRecording(SurfaceHolder surfaceHolder, SurfaceView surfaceView) {

        if (!isRecording) {
            if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_ACCELERATION, true)) {
                AccelerationSensorListener accListener = new AccelerationSensorListener(this.getApplicationContext(), realTimeTransferEnabled);
                LinearAccelerationSensorListener laccListener = new LinearAccelerationSensorListener(this.getApplicationContext(), realTimeTransferEnabled);
                sensorList.add(accListener);
                sensorList.add(laccListener);
            }
            if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_GPS, true)) {
                GPSSensorListener gpsListener = new GPSSensorListener(this.getApplicationContext(),realTimeTransferEnabled);
                sensorList.add(gpsListener);
            }
            if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_BAROMETER, true)) {
                BarometerSensorListener barListener = new BarometerSensorListener(this.getApplicationContext(),realTimeTransferEnabled);
                sensorList.add(barListener);
            }
            if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_GYRO, true)) {
                GyroscopeSensorListener gyroListener = new GyroscopeSensorListener(this.getApplicationContext(),realTimeTransferEnabled);
                sensorList.add(gyroListener);
            }
            if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_COMPASS, true)) {
                CompassSensorListener compassListener = new CompassSensorListener(this.getApplicationContext(),realTimeTransferEnabled);
                sensorList.add(compassListener);
            }
            if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_CRASHRECOGNITION, true)) {
                CrashDetectorListener crashDetectorListener = new CrashDetectorListener(this.getApplicationContext());
                sensorList.add(crashDetectorListener);
            }

            //Camera
            if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_VIDEO, true)) {
                CameraSensorListener cameraSensorListener = new CameraSensorListener(this.getApplicationContext(), surfaceHolder, surfaceView);
                sensorList.add(cameraSensorListener);
            }

            //deleter service
            if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_LOOPRECORDING, true)) {
                Integer loopCycleTime = Integer.valueOf(sharedPref.getString(Constants.SETTINGS.KEY_PREF_LOOPLENGTH, "5"));
                Integer partLength = Integer.valueOf(sharedPref.getString(Constants.SETTINGS.KEY_PREF_PARTLENGTH, "30"));
                loopCycleDeleter = new LoopCycleDeleter(loopCycleTime, partLength);
            }

            // Start all the sensors incl. camera
            for (SensorInterface sensor : sensorList) {
                sensor.start();
            }

            // set handler to restart sensors for loop if enabled
            if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_LOOPRECORDING, false)) {
                String intervalString = sharedPref.getString(Constants.SETTINGS.KEY_PREF_PARTLENGTH, "30");
                int interval = Integer.valueOf(intervalString) * 1000;
                sensorRecordRestartHandler.postDelayed(restartSensorRecording, interval);
            }


            //Toast
            Toast.makeText(getBaseContext(), "Recording Started", Toast.LENGTH_SHORT).show();
            isRecording = true;
        } else {
            Toast.makeText(getBaseContext(), "Recording already running!", Toast.LENGTH_SHORT).show();
        }
    }

    /*
     * Method which stops all the SensorListeners
     *  - clearLastLoopCycle: delete last loopCycle
     */

    public void stopRecording(Boolean clearLastLoopCycle) {
        if (isRecording) {

            // stop restarting handler if loopRecording enabled
            //
            if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_LOOPRECORDING, false)) {
                try {
                    sensorRecordRestartHandler.removeCallbacks(restartSensorRecording);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    Toast.makeText(this, "Failed RestartHandler remove Callback", Toast.LENGTH_LONG);
                }
            }


            //stop all other sensors incl. camera
            for (SensorInterface sensor : sensorList) {
                sensor.stop();
            }
            //Clear the list
            sensorList.clear();

            //Clear Nearby
            if(realTimeTransferEnabled){
                realTimeSender.stopTransfer();
            }

            // Toast
            Toast.makeText(getBaseContext(), "Recording Stopped", Toast.LENGTH_SHORT).show();
            isRecording = false;
        } else {
            Toast.makeText(getBaseContext(), "can't stop recording. recording not started!", Toast.LENGTH_SHORT).show();
        }

        // Unregister Realtime local Broadcast receiver
        if(realTimeTransferEnabled) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        }

        // stop loopCycleDeleter if loopRecording enabled
        if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_LOOPRECORDING, false)) {
            if (clearLastLoopCycle) {
                if (loopCycleDeleter != null) {
                    loopCycleDeleter.clearAndStop();
                    loopCycleDeleter = null;
                    Toast.makeText(this, "Try to clear loopCycleDeleter..", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "ERROR: loopCycleDeleter is NULL!", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (loopCycleDeleter != null) {
                    loopCycleDeleter.stop();
                    loopCycleDeleter = null;
                    Toast.makeText(this, "Try to just stop loopCycleDeleter..", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "ERROR: loopCycleDeleter is NULL!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (clearLastLoopCycle){
            Toast.makeText(this, "ERROR: loopCycleDeleter is NULL! But called with clearLastLoopCycle. CHECK IF ALL DATA THAT SHOULD BE DELETED IS DELETED!", Toast.LENGTH_SHORT).show();
        }

        stopForeground(true);
        stopSelf();
        Toast.makeText(this, "recording stopped!", Toast.LENGTH_SHORT).show();
    }


    //Method for restarting Sensors
    private Runnable restartSensorRecording = new Runnable() {
        @Override
        public void run() {
            for (SensorInterface sensor : sensorList) {
                sensor.restart();
            }
            String intervalString = sharedPref.getString(Constants.SETTINGS.KEY_PREF_PARTLENGTH, "30");
            int interval = Integer.valueOf(intervalString) * 1000;
            sensorRecordRestartHandler.postDelayed(restartSensorRecording, interval);
        }
    };


    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.i(TAG, "onDestroy of RecordingService is called");
    }

    /*
------------------------------------------------------------------------------------------------

                launchAlarm

                This Method launches an Alarm => Shows an Alert with the options:
                - "Yes" (Save Data, launch HotSpot)
                - "No" (False-positive)

                If user doesn't react to the alert Dialog within 15 seconds:
                - Save the Data
                - launch the hotspot.


------------------------------------------------------------------------------------------------
 */
    private void launchAlarm(String reason) {
        if (isInAlarmMode) {
            //Do Nothing since we are already in alarm mode.
        } else {
            isInAlarmMode = true;
           //starting alarm dialog to ask driver
            Intent startAlarmDialogIntent = new Intent(RecordingService.this, AlarmDialog.class);
            //But the Reason for the alarm as Extra
            startAlarmDialogIntent.putExtra("reason", reason);

            startAlarmDialogIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startAlarmDialogIntent);
        }
    }

    private void crashNoReaction() {
        crashSaveData();
        if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_HOTSPOT, false)) {
            launchHotspot();
        }
        isInAlarmMode = false;
    }

    private void crashSaveData() {
        //Restart the DeleterService if loopRecording enabled
        if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_LOOPRECORDING, false)) {
            try {
                loopCycleDeleter.restart();
                Toast.makeText(getApplicationContext(), "called restart of loopCycleDeleter", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                Toast.makeText(getApplicationContext(), "loopCycle restarting failed", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Toast.makeText(getApplicationContext(), "loopCycle not active. no action needed to prevent from deleting", Toast.LENGTH_SHORT).show();
        }

        isInAlarmMode = false;
    }

    private void crashFalsePositive() {
        // No => False Positive => Nothing happens
        Toast.makeText(getApplicationContext(), "False Positive", Toast.LENGTH_SHORT).show();
        isInAlarmMode = false;
    }



    /*
------------------------------------------------------------------------------------------------

            launchHotspot
            Just use in case of crash:
            - runs the emergency hotspot

------------------------------------------------------------------------------------------------
*/
    public void launchHotspot(){
        if(isRecording){stopRecording(false);}

        Toast.makeText(getApplicationContext(), "stopped recording. start to prepare hotspot!", Toast.LENGTH_SHORT).show();
        if(mHotSpotSender == null){
            mHotSpotSender = new HotSpotSender(getApplicationContext());
        }

        mHotSpotSender.activateHotspot();

    }


    /*
------------------------------------------------------------------------------------------------

            reattachRecordingNotification
            - clear all previous notifications
            - set notifications for api > 25 or api <= 25

------------------------------------------------------------------------------------------------
*/

    public void reattachRecordingNotification(){
        stopForeground(true);

        // for best practice we have to set notificationChannel api > 25
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel notificationChannel = new NotificationChannel(Constants.ID.RECORDING_NOTIFICATION_CHANNEL_ID, Constants.ID.RECORDING_NOTIFICATION_CHANNEL_ID, NotificationManager.IMPORTANCE_HIGH);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        //notification intent
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction("AndroidBlackbox");
        //notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        //CRASH button intent
        Intent crashIntent = new Intent(this, RecordingService.class);
        crashIntent.setAction(Constants.ACTION.ALARM_ACTION_NOTIFICATION);
        PendingIntent pCrashIntent = PendingIntent.getService(this, 0, crashIntent, 0);


        //stop recording button intent
        Intent stopRecordingIntent = new Intent(this, RecordingService.class);
        stopRecordingIntent.setAction(Constants.ACTION.STOP_RECORDING_ACTION);
        PendingIntent pStopRecordingIntent = PendingIntent.getService(this, 0, stopRecordingIntent, 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher);

        NotificationCompat.Action crashAction = new NotificationCompat.Action.Builder(android.R.drawable.ic_dialog_alert, "CRASH! (KEEP DATA)", pCrashIntent).build();
        NotificationCompat.Action stopAction = new NotificationCompat.Action.Builder(android.R.drawable.ic_lock_power_off, "Stop recording", pStopRecordingIntent).build();


        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, Constants.ID.RECORDING_NOTIFICATION_CHANNEL_ID)
                .setContentTitle("Android Blackbox")
                .setTicker("recording started!")
                .setContentText("recording...")
                .setSmallIcon(R.drawable.icon_recording_start)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .addAction(crashAction)
                .addAction(stopAction);

        Notification notification = builder.build();

        startForeground(Constants.ID.RECORDING_SERVICE_ID, notification);
    }
}