package ch.bfh.ti.bachelor.android_blackbox.Util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.os.Build;
import android.support.v7.preference.PreferenceManager;
import android.widget.Toast;

import ch.bfh.ti.bachelor.android_blackbox.RecordingService;

//Oreo or above: https://commonsware.com/blog/2017/04/11/android-o-implicit-broadcast-ban.html
// 2018-10-22 14:53:03.537 5213-8828/? W/BroadcastQueue: Background execution not allowed: receiving Intent { act=android.intent.action.ACTION_POWER_CONNECTED flg=0x4000010 (has extras) } to ch.bfh.students.schmm11.androidblackboxprototype/.PowerConnectionReceiver
//2018-10-22 14:53:04.141 5213-7205/? W/BroadcastQueue: Background execution not allowed: receiving Intent { act=android.intent.action.ACTION_POWER_DISCONNECTED flg=0x4000010 } to ch.bfh.students.schmm11.androidblackboxprototype/.PowerConnectionReceiver
//2018-10-22 14:53:04.642 5213-7205/? W/BroadcastQueue: Background execution not allowed: receiving Intent { act=android.intent.action.ACTION_POWER_CONNECTED flg=0x4000010 (has extras) } to ch.bfh.students.schmm11.androidblackboxprototype/.PowerConnectionReceiver
//2018-10-22 14:53:05.507 5213-7760/? W/BroadcastQueue: Background execution not allowed: receiving Intent { act=android.intent.action.ACTION_POWER_DISCONNECTED flg=0x4000010 } to ch.bfh.students.schmm11.androidblackboxprototype/.PowerConnectionReceiver


public class PowerConnectionReceiver extends BroadcastReceiver {
    private static final String TAG = "PowerConnectionReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        Context c = context.getApplicationContext();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        if (sharedPref.getBoolean(Constants.SETTINGS.KEY_PREF_AUTOSTART, false)) {

            IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = c.registerReceiver(null, filter);

            int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            Toast.makeText(context, "PowerconnectionReceiver (charging state: " + chargePlug + ") ", Toast.LENGTH_SHORT).show();



            if (chargePlug > 0) {
                //start recording
                Intent startIntent = new Intent(context, RecordingService.class);
                startIntent.setAction(Constants.ACTION.START_RECORDING_ACTION);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    c.startForegroundService(startIntent);
                }else {
                    c.startService(startIntent);
                }
            } else if(chargePlug == 0){
                //stop recording
                Intent stopIntent = new Intent(context, RecordingService.class);
                stopIntent.setAction(Constants.ACTION.STOP_RECORDING_ACTION);
                c.startService(stopIntent);
            }
        }
    }
}
