package ch.bfh.ti.bachelor.android_blackbox.Dialog;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import ch.bfh.ti.bachelor.android_blackbox.RecordingService;
import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;

public class ArrivedSafelyDialog extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_delete_last_loop_cycle_dialog);
    }

    @Override
    protected void onStart() {
        super.onStart();


        //Create the alert dialog
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Arrived safely")
                .setMessage("Have you arrived safely? If you are, all unsaved data from this session will get deleted!")
                .setPositiveButton("Yes (Delete)", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes => stop recording and purge last loop cycle
                        Intent crashSaveDataIntent = new Intent(ArrivedSafelyDialog.this, RecordingService.class);
                        crashSaveDataIntent.setAction(Constants.ACTION.STOP_RECORDING_CLEAR_LOOP);
                        startService(crashSaveDataIntent);
                        dialog.cancel();
                        finish();
                    }
                })
                .setNegativeButton("No (Record further)", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // No => don't stop recording, do Nothing
                        dialog.cancel();
                        finish();
                    }
                })
                .create();
        // Create a Listener with a Timer to know the passed Time
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                final Button negativeButton = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE);
                final CharSequence positiveButtonText = negativeButton.getText();
                new CountDownTimer(Constants.STATICS.ALARM_TRESHOLD_IN_MS, 100) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        negativeButton.setText(String.format(Locale.getDefault(), "%s (%d)",
                                positiveButtonText,
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) + 1));
                    }

                    @Override
                    public void onFinish() {
                        if (((AlertDialog) dialog).isShowing()) {
                            //user did not reply to dialog Alert => keep recording
                            dialog.cancel();
                            finish();
                        }
                    }
                }.start();
            }
        });
        dialog.show();
        //do not finish dialog if user touch outside of the dialog
        dialog.setCanceledOnTouchOutside(false);
    }
}
