package ch.bfh.ti.bachelor.android_blackbox.Util;

import android.os.Environment;
import android.util.Log;

import java.io.File;

public class DataPurger {
    private static final String TAG = "DataPurger";

    public boolean purgeData(){
        if (isExternalStorageWritable()) {
            Log.e(TAG, " Writable");
            try {
                File file = new File(Environment.getExternalStorageDirectory() + Constants.PATHS.DATA_ROOT_PATH);
                return deleteRecursive(file);
            }catch (Exception e){
                Log.e(TAG, e.getMessage());
                return false;
            }
        } else {
            Log.e(TAG, "Not Writable");
            return false;
        }
    }

    // delete all files and folders recursively
    private boolean deleteRecursive (File fileOrDirectory){
        if(fileOrDirectory.isDirectory()){
            for(File child : fileOrDirectory.listFiles()){
                deleteRecursive(child);
            }
        }
        return fileOrDirectory.delete();
    }


    /* Checks if external storage is available for read and write */
    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
}
