package ch.bfh.ti.bachelor.android_blackbox.Sensor;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import ch.bfh.ti.bachelor.android_blackbox.Saver.GPSSaver;
import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;

public class GPSSensorListener implements SensorInterface, LocationListener {
    private Sensor sensor;
    private GPSSaver saver = new GPSSaver();
    private static final String TAG = "Blackbox_GPSListener";
    LocationManager locationManager;
    private String strToSave = "";
    private double latitude;
    private double longitude;
    private double altitude;
    private float speed;
    private Boolean realTimeEnabled;
    private String intentAction = Constants.ACTION.REALTIME_TRANSFER_SEND;
    private LocalBroadcastManager localBroadcastManager;


    @SuppressLint("MissingPermission")
    public GPSSensorListener(Context context, Boolean realTimeEnabled){
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        this.realTimeEnabled = realTimeEnabled;

        if(realTimeEnabled){
            localBroadcastManager = LocalBroadcastManager.getInstance(context);
        }


    }


    @SuppressLint("MissingPermission")
    @Override
    public void start() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,this);
        saver.save("Timestamp [ms]; Latitude [Degrees]; Longitude [Degrees]; Altitude (WGS 84)[m]; Speed [m/s]"+ System.getProperty ("line.separator")); //headlines
    }

    @Override
    public void stop() {
        locationManager.removeUpdates(this);
    }

    @Override
    public void restart() {
        saver = new GPSSaver();
        stop();
        start();
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.v(TAG, "GPS onLocationChanged received");
        long timestamp = System.currentTimeMillis();

        latitude= location.getLatitude();
        longitude = location.getLongitude();
        altitude = location.getAltitude();
        speed = location.getSpeed();

        strToSave = timestamp  + ";" + latitude + ";"  + longitude + ";" + altitude + ";" + speed + System.getProperty ("line.separator");
        saver.save(strToSave);

        /*
        When RealTimeTransfer is enabled: Send it
         */
        if(realTimeEnabled){
            // Send Data here
            Intent localIntent = new Intent(intentAction);
            localIntent.putExtra("data", "gps;" + strToSave);

            localBroadcastManager.sendBroadcast(localIntent);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
