package ch.bfh.ti.bachelor.android_blackbox.Dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import ch.bfh.ti.bachelor.android_blackbox.RecordingService;
import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;

public class AlarmDialog extends AppCompatActivity {

    //Stores the reason of the launched Alarm.
    private String alarmReason;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        alarmReason = getIntent().getExtras().getString("reason");
        //setContentView(R.layout.activity_alarm_dialog);

        //set dialog theme to activity
        //setTheme(android.R.style.Theme_Dialog);  //now in the manifest!
    }


    @Override
    protected void onStart() {
        super.onStart();
        playToneAndVibrate();


        //Create the alert dialog
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Alarm")
                .setMessage("The Device measured an event. Shall we save the data?" + System.getProperty("line.separator") + "Reason: "+ System.getProperty("line.separator") +alarmReason)
                .setPositiveButton("Yes (Save)", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent crashSaveDataIntent = new Intent(AlarmDialog.this, RecordingService.class);
                        crashSaveDataIntent.setAction(Constants.ACTION.CRASH_SAVE_DATA_ACTION);
                        startService(crashSaveDataIntent);
                        dialog.cancel();
                        finish();
                    }
                })
                .setNegativeButton("No (False-positive)", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // No => False Positive => Nothing happens
                        Intent falsePositiveIntent = new Intent(AlarmDialog.this, RecordingService.class);
                        falsePositiveIntent.setAction(Constants.ACTION.CRASH_FALSE_POSITIVE_ACTION);
                        startService(falsePositiveIntent);
                        dialog.cancel();
                        finish();
                    }
                })
                .create();
        // Create a Listener with a Timer to know the passed Time
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                final Button positiveButton = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                final CharSequence positiveButtonText = positiveButton.getText();
                new CountDownTimer(Constants.STATICS.ALARM_TRESHOLD_IN_MS, 100) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        positiveButton.setText(String.format(Locale.getDefault(), "%s (%d)",
                                positiveButtonText,
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) + 1));
                    }

                    @Override
                    public void onFinish() {
                        if (((AlertDialog) dialog).isShowing()) {
                            //user did not reply to dialog Alert.
                            Intent crashNoReactionIntent = new Intent(AlarmDialog.this, RecordingService.class);
                            crashNoReactionIntent.setAction(Constants.ACTION.CRASH_NO_REACTION_ACTION);
                            startService(crashNoReactionIntent);
                            playToneAndVibrate();
                            dialog.cancel();
                            finish();
                        }
                    }
                }.start();
            }
        });
        dialog.show();
        //do not finish dialog if user touch outside of the dialog
        dialog.setCanceledOnTouchOutside(false);
    }

    private void playToneAndVibrate() {
        //Play a Sound and vibrate, get the users attention
        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
        toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 1000);
        Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(1000);
    }
}