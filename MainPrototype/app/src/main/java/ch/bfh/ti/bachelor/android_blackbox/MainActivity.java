package ch.bfh.ti.bachelor.android_blackbox;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;


import java.io.File;

import ch.bfh.ti.bachelor.android_blackbox.HotSpot.HotSpotReceiver;
import ch.bfh.ti.bachelor.android_blackbox.RealTimeTransfer.RealTimeTransferReceiverActivity;
import ch.bfh.ti.bachelor.android_blackbox.Settings.SettingsActivity;
import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;
import ch.bfh.ti.bachelor.android_blackbox.Util.DataPurger;
import ch.bfh.ti.bachelor.android_blackbox.Util.RequestPermissions;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    // RecordingService
    private Boolean isRecording = false;

    //Buttons
    private ImageButton btn_Record, btn_Crash;
    private ImageView iv_StatusNearby, iv_StatusAcceleromter, iv_StatusLocation, iv_StatusBarometer, iv_StatusGyro, iv_StatusCompass, iv_StatusVideo, iv_StatusAudio;

    //Preferences
    private SharedPreferences sharedPref;

    //camera
    public static SurfaceView mSurfaceView;
    public static SurfaceHolder mSurfaceHolder;

    //real time service
    private Boolean isNearbyAdvertising = false;
    private Boolean isNearbyConnected = false;

    //BroadcastManager to request gui updates
    private LocalBroadcastManager localBroadcastManager;


    //Broadcast for Nearby Status Updates
    /*
    Init Receiver for local Broadcasts
     */
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == Constants.ACTION.REALTIME_RECEIVER_GUI_UPDATE){
                isNearbyAdvertising = intent.getBooleanExtra("isNearbyAdvertising",false);
                isNearbyConnected = intent.getBooleanExtra("isNearbyConnected",false);
                changeStatusImages();
            }
            else{
                Log.wtf(TAG,"Received an impossible Intent Action: "+intent.getAction());
            }
        }




    };


    // ------------------------------------------ END INITIALISATIONS ------------------------------





    /*
    ------------------------------------------------------------------------------------------------

                    onCreate   (onCreate - onStart - onResume)

    ------------------------------------------------------------------------------------------------
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Settings:
        // Set the default settings the first time the app is started
        PreferenceManager.setDefaultValues(this.getApplicationContext(), R.xml.preferences, false);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());

        // Call setTheme before creation of any(!) View.
        if ( sharedPref.getBoolean (Constants.SETTINGS.KEY_PREF_DARKTHEME, false)){
            setTheme(android.R.style.Theme_Material_NoActionBar);
        }
        setContentView(R.layout.activity_main);


        //check if RecordingService is running and set isRecording
        if(isMyServiceRunning(RecordingService.class)){
            isRecording = true;
        } else {
            isRecording = false;
        }

        //init local broadcastmanager to send gui update requests
        localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());


        // set toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Let the Screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //print versions
        setTitle("Android Blackbox (Beta " + BuildConfig.VERSION_NAME + ")");


        //set landscape mode
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //Quick and dirty Asking for Permissions.
        RequestPermissions myPermReq = new RequestPermissions();
        myPermReq.requestAllPermissions(this);

        //camera surface
        mSurfaceView = findViewById(R.id.surfaceView);
        mSurfaceHolder = mSurfaceView.getHolder();

        //Init every Status Image
        this.iv_StatusNearby = findViewById(R.id.iv_StatusNearby);
        this.iv_StatusAcceleromter = findViewById(R.id.iv_StatusAccelerometer);
        this.iv_StatusLocation = findViewById(R.id.iv_StatusLocation);
        this.iv_StatusBarometer = findViewById(R.id.iv_StatusBarometer);
        this.iv_StatusGyro = findViewById(R.id.iv_StatusGyro);
        this.iv_StatusCompass = findViewById(R.id.iv_StatusCompass);
        this.iv_StatusVideo = findViewById(R.id.iv_StatusVideo);
        this.iv_StatusAudio = findViewById(R.id.iv_StatusAudio);

        initializeButtons();

    } // end onCreate()

    /*
    ------------------------------------------------------------------------------------------------

                    onStart   (onCreate - onStart - onResume)

    ------------------------------------------------------------------------------------------------
     */
    @Override
    public void onStart() {
        super.onStart();
    }


    /*
    ------------------------------------------------------------------------------------------------

                    onResume   (onCreate - onStart - onResume)

    ------------------------------------------------------------------------------------------------
     */

    @Override
    public void onResume() {
        super.onResume();

        // register local broadcast receiver
        IntentFilter filter = new IntentFilter(Constants.ACTION.REALTIME_RECEIVER_GUI_UPDATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter);


        //send gui update request
        sendRequestToRealTimeTransferForGuiUpdate();

        //check if RecordingService is running and set isRecording
        if(isMyServiceRunning(RecordingService.class)){
            isRecording = true;
        } else {
            isRecording = false;
        }

        //Change the status icons
        changeStatusImages();
    }



    /*
    ------------------------------------------------------------------------------------------------

                    onPause   (onPause - onStop - onDestroy)

    ------------------------------------------------------------------------------------------------
     */
    @Override
    protected void onPause() {
        super.onPause();

        // unregister local broadcast receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);

    }

    /*
    ------------------------------------------------------------------------------------------------

                    onStop   (onPause - onStop - onDestroy)

    ------------------------------------------------------------------------------------------------
     */
    @Override
    protected void onStop() {
        super.onStop();
    }
    /*
    ------------------------------------------------------------------------------------------------

                    onDestroy   (onPause - onStop - onDestroy)

    ------------------------------------------------------------------------------------------------
     */
    @Override
    public void onDestroy() {
        super.onDestroy();

    }



        /*
    ------------------------------------------------------------------------------------------------

                    the options menu

    ------------------------------------------------------------------------------------------------
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.btn_Settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.btn_Purge){
            //Alert dialog for purging (Yes/No Box)
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            final  DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            DataPurger dataPurger = new DataPurger();
                            if(isRecording){
                                //Stop Recording
                                Intent stopIntent = new Intent(MainActivity.this, RecordingService.class);
                                stopIntent.setAction(Constants.ACTION.STOP_RECORDING_ACTION);
                                startService(stopIntent);
                            }

                            File file = new File(Environment.getExternalStorageDirectory() + Constants.PATHS.DATA_ROOT_PATH);
                            if(file.exists()) {
                                if (dataPurger.purgeData()) {
                                    Toast.makeText(MainActivity.this, "Data purged!", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(MainActivity.this, "Error while purging! Data NOT purged!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(MainActivity.this, "Root path doesn't exist! Already purged?", Toast.LENGTH_LONG).show();
                            }
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            Toast.makeText(MainActivity.this, "Aborted purging! Data NOT purged!", Toast.LENGTH_LONG).show();
                            break;
                    }
                }
            };

            builder.setTitle("Are you sure?")
                    .setMessage("This will delete ALL Data stored in the folder AndroidBlackbox!")
                    .setPositiveButton("Yes",dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }
        if (id == R.id.btn_HotspotReceiver) {
            Intent intent = new Intent(this, HotSpotReceiver.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.btn_RealTimeReceiver) {
            Intent intent = new Intent(this, RealTimeTransferReceiverActivity.class);
            startActivity(intent);
            return true;
        }
        if(id == R.id.btn_OpenDirectory){
            // doesn't work much better: https://stackoverflow.com/questions/50072638/fileuriexposedexception-in-android/50102119#50102119
            Toast.makeText(MainActivity.this, "Please select a file browser. Other apps may crash!", Toast.LENGTH_LONG).show();
            PackageManager packageManager = getPackageManager();
            Uri url = Uri.parse(Environment.getExternalStorageDirectory() + Constants.PATHS.DATA_ROOT_PATH);
            Intent intent = new Intent();
            intent.setDataAndType(url,"*/*");
            intent.setAction(Intent.ACTION_VIEW);
            if (intent.resolveActivity(packageManager) != null) {
                startActivity(Intent.createChooser(intent, "Open folder"));
            } else {
                Toast.makeText(MainActivity.this, "No app want open the data directory! (mimetype */*)", Toast.LENGTH_LONG).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }




    /*
    ------------------------------------------------------------------------------------------------

                    changeStatusImages
                    - change the icons to match the selected sensors in settings
                    - change the record button

    ------------------------------------------------------------------------------------------------
     */
    private void changeStatusImages(){
        //Nearby
        if ( sharedPref.getBoolean (Constants.SETTINGS.KEY_PREF_REALTIMETRANSFER, false)){
            if(isNearbyConnected) {
                iv_StatusNearby.setImageResource(R.drawable.icon_nearby_connected);
            }else if(isNearbyAdvertising){
                iv_StatusNearby.setImageResource(R.drawable.icon_nearby_hotspot_advertising);
            }else {
                iv_StatusNearby.setImageResource(R.drawable.icon_nearby);
            }
        } else{
            iv_StatusNearby.setImageResource(R.drawable.icon_nearby_inactive);
        }
        //Acceleration
        if ( sharedPref.getBoolean (Constants.SETTINGS.KEY_PREF_ACCELERATION, true)){
            iv_StatusAcceleromter.setImageResource(R.drawable.icon_acceleration_active);
        } else{
            iv_StatusAcceleromter.setImageResource(R.drawable.icon_acceleration);
        }
        //GPS
        if ( sharedPref.getBoolean (Constants.SETTINGS.KEY_PREF_GPS, true)){
            iv_StatusLocation.setImageResource(R.drawable.icon_location_active);
        }else {
            iv_StatusLocation.setImageResource(R.drawable.icon_location);
        }
        //Barometer
        if ( sharedPref.getBoolean (Constants.SETTINGS.KEY_PREF_BAROMETER, true)){
            iv_StatusBarometer.setImageResource(R.drawable.icon_barometer_active);
        }else {
            iv_StatusBarometer.setImageResource(R.drawable.icon_barometer);
        }
        //Gyro
        if ( sharedPref.getBoolean (Constants.SETTINGS.KEY_PREF_GYRO, true)){
            iv_StatusGyro.setImageResource(R.drawable.icon_gyroscope_active);
        }else {
            iv_StatusGyro.setImageResource(R.drawable.icon_gyroscope);
        }
        //Compass
        if ( sharedPref.getBoolean (Constants.SETTINGS.KEY_PREF_COMPASS, true)){
            iv_StatusCompass.setImageResource(R.drawable.icon_compass_active);
        }else {
            iv_StatusCompass.setImageResource(R.drawable.icon_compass);
        }
        //Video
        if ( sharedPref.getBoolean (Constants.SETTINGS.KEY_PREF_VIDEO, true)){
            iv_StatusVideo.setImageResource(R.drawable.icon_video_active);
        }else {
            iv_StatusVideo.setImageResource(R.drawable.icon_video);
        }
        //Audio
        if ( sharedPref.getBoolean (Constants.SETTINGS.KEY_PREF_AUDIO, true)){
            iv_StatusAudio.setImageResource(R.drawable.icon_audio_active);
        }else {
            iv_StatusAudio.setImageResource(R.drawable.icon_audio);
        }
        //Record Button
        if (isRecording){
            btn_Record.setImageResource(R.drawable.icon_recording_stop);
        }else{
            btn_Record.setImageResource(R.drawable.icon_recording_start);
        }
    }




    /*
------------------------------------------------------------------------------------------------

     isMyServicerunning
     checks if a specific Service is running

------------------------------------------------------------------------------------------------
*/
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }



    /*
------------------------------------------------------------------------------------------------

    initializeButtons
    - record button
    - crash button

------------------------------------------------------------------------------------------------
*/

    private void initializeButtons(){
        // Record Button
        this.btn_Record = findViewById(R.id.btn_Record);
        btn_Record.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                    if(!isRecording){

                    //start service
                    Intent startIntent = new Intent(MainActivity.this, RecordingService.class);
                    startIntent.setAction(Constants.ACTION.START_RECORDING_ACTION);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                       startForegroundService(startIntent);
                    }else {
                        startService(startIntent);
                    }

                    isRecording = true;
                    btn_Record.setImageResource(R.drawable.icon_recording_stop);
                    changeStatusImages();
                    Log.d(TAG, "Recording Button pressed => Start");

                }else if(isRecording){

                    //Stop Recording
                    Intent stopIntent = new Intent(MainActivity.this, RecordingService.class);
                    stopIntent.setAction(Constants.ACTION.STOP_RECORDING_ACTION);
                    startService(stopIntent);


                    isRecording = false;
                    btn_Record.setImageResource(R.drawable.icon_recording_start);
                    changeStatusImages();
                    Log.d(TAG, "Recording Button pressed => Stop");
                }
            }
        });

        // Crash Button
        this.btn_Crash = findViewById(R.id.btn_Crash);
        btn_Crash.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if (isRecording) {
                    // send intent to RecordingService to SAVE DATA
                    Intent saveDataIntent = new Intent(MainActivity.this, RecordingService.class);
                    saveDataIntent.putExtra("reason", "Manually pressed Alarm Button");
                    saveDataIntent.setAction(Constants.ACTION.ALARM_ACTION);
                    startService(saveDataIntent);
                }
            }
        });

        //Init every Status Image to show toast

        iv_StatusNearby.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Real time transfer", Toast.LENGTH_SHORT).show();
            }
        });

        iv_StatusAcceleromter.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Accelerometer", Toast.LENGTH_SHORT).show();
            }
        });

        iv_StatusLocation.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "GPS", Toast.LENGTH_SHORT).show();
            }
        });
        iv_StatusBarometer.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Barometer", Toast.LENGTH_SHORT).show();
            }
        });
        iv_StatusGyro.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Gyroscope", Toast.LENGTH_SHORT).show();
            }
        });
        iv_StatusCompass.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Compass", Toast.LENGTH_SHORT).show();
            }
        });
        iv_StatusVideo.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Video", Toast.LENGTH_SHORT).show();
            }
        });
        iv_StatusAudio.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Audio", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*
------------------------------------------------------------------------------------------------

sendRequestToRealTimeTransferForGuiUpdate
- sends a gui update request to real time sender

------------------------------------------------------------------------------------------------
*/
    public void sendRequestToRealTimeTransferForGuiUpdate(){
        Intent localIntent = new Intent(Constants.ACTION.REALTIME_RECEIVER_REQUEST_GUI_UPDATE);
        localBroadcastManager.sendBroadcast(localIntent);
    }

}
