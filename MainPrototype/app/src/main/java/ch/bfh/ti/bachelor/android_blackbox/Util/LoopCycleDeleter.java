package ch.bfh.ti.bachelor.android_blackbox.Util;

import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

public class LoopCycleDeleter {

    private static final String TAG = "LoopCycleDeleter";

    private int loopTime;
    private Date startDate;
    private Date loopTimeDate;
    private int partLength;
    private Handler deleteHandler = new Handler();

    public LoopCycleDeleter(Integer loopTimePreference, Integer partLength) {
        this.startDate = Calendar.getInstance().getTime();
        this.partLength = partLength;
        loopTime = loopTimePreference;
        loopTimeDate = new Date(0, 0, 0, 0, loopTime);

        deleteHandler.post(deleteOutdatedFiles);
    }

    public void restart(){

        /*
        Update the startDate to now + the part lenght
        if we dont add the part lenght, the Deleter deletes the last Loop before the alarm Event....
         */
        this.startDate = Calendar.getInstance().getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.SECOND, partLength);
        startDate = calendar.getTime();
    }

    public void clearAndStop(){
        deleteLastLoopCycle();
        stop();
    }

    public void stop(){
        //clean a last time then remove callbacks
        deleteHandler.post(deleteOutdatedFiles);
        deleteHandler.removeCallbacks(deleteOutdatedFiles);
    }

    /*

     */
    private Runnable deleteOutdatedFiles = new Runnable() {
        @Override
        public void run() {

            try {
                File file = new File(Environment.getExternalStorageDirectory() + Constants.PATHS.RECORD_SAVE_PATH);

                //           | Modified previous |       |start<=====|modified=======|modified====DELETE SECTION========>(------------------loopTime---------|now)

                if (file.isDirectory()) {
                    for (File child : file.listFiles()) {
                        if (!child.isDirectory()) {
                            Date lastModDate = new Date(child.lastModified());
                            long lastModTimeMillis = lastModDate.getTime();
                            long startDateTimeMillis = startDate.getTime();
                            long nowTimeMillis = Calendar.getInstance().getTimeInMillis();
                            long loopTimeMillis = loopTime * 60000; //looptime is a value in minutes
                            long deleteOlderThan = nowTimeMillis - loopTimeMillis;

                            long deleteYoungerThan = startDateTimeMillis;

                            if (lastModTimeMillis > deleteYoungerThan && lastModTimeMillis < deleteOlderThan) {
                                child.delete();
                                Log.d(TAG, "Deleted: " + child.getPath());
                            }
                        }
                    }
                }
            } catch (Exception e){
                Log.e(TAG, "cant delete outdated files");
            }
            int interval = 5000;
            deleteHandler.postDelayed(deleteOutdatedFiles, interval);
        }
    };


    /*
    Deltes the last LoopCycle => Used when user arrives without accident.
     */
    private void deleteLastLoopCycle() {
        try {
            File file = new File(Environment.getExternalStorageDirectory() + Constants.PATHS.RECORD_SAVE_PATH);
            if (file.isDirectory()) {
                for (File child : file.listFiles()) {
                    if (!child.isDirectory()) {
                        Date lastModDate = new Date(child.lastModified());
                        long lastModTimeMillis = lastModDate.getTime();
                        long startDateTimeMillis = startDate.getTime();
                        long deleteYoungerThan = startDateTimeMillis;

                        if (lastModTimeMillis > deleteYoungerThan) {
                            child.delete();
                            Log.d(TAG, "Deleted: " + child.getPath());
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "cant delete last loopCycle");
        }
    }
}