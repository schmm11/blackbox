package ch.bfh.ti.bachelor.android_blackbox.RealTimeTransfer;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo;
import com.google.android.gms.nearby.connection.DiscoveryOptions;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.UnsupportedEncodingException;

import ch.bfh.ti.bachelor.android_blackbox.R;
import ch.bfh.ti.bachelor.android_blackbox.RealTimeTransfer.RealTimeSavers.RealTimeAccelerationSaver;
import ch.bfh.ti.bachelor.android_blackbox.RealTimeTransfer.RealTimeSavers.RealTimeBarometerSaver;
import ch.bfh.ti.bachelor.android_blackbox.RealTimeTransfer.RealTimeSavers.RealTimeCompassSaver;
import ch.bfh.ti.bachelor.android_blackbox.RealTimeTransfer.RealTimeSavers.RealTimeGPSSaver;
import ch.bfh.ti.bachelor.android_blackbox.RealTimeTransfer.RealTimeSavers.RealTimeGyroscopeSaver;

import ch.bfh.ti.bachelor.android_blackbox.RealTimeTransfer.RealTimeSavers.RealTimeLinearAccelerationSaver;
import ch.bfh.ti.bachelor.android_blackbox.Util.Constants;


public class RealTimeTransferReceiverService extends Service {
    private static final String TAG = "RTT_Service";
    private String serviceId;
    private LocalBroadcastManager localBroadcastManager;

    //Savers
    private RealTimeAccelerationSaver realTimeAccelerationSaver;
    private RealTimeLinearAccelerationSaver realTimeLinearAccelerationSaver;
    private RealTimeBarometerSaver realTimeBarometerSaver;
    private RealTimeCompassSaver realTimeCompassSaver;
    private RealTimeGPSSaver realTimeGPSSaver;
    private RealTimeGyroscopeSaver realTimeGyroscopeSaver;

    //Nearby
    private Strategy strategy = Strategy.P2P_POINT_TO_POINT;
    private String partnerEndpointName;
    private ConnectionsClient mConnectionsClient;
    private int amountPayloads = 0;
    private Boolean nearbyConnected = false;

    private Boolean nearbyAdvertising = false;

    //Nearby Methods
    //Nearby Connection Lifecycle
    private final ConnectionLifecycleCallback mConnectionLifecycleCallback =
            new ConnectionLifecycleCallback() {
                @Override
                public void onConnectionInitiated(String endpointId, ConnectionInfo connectionInfo) {
                    // Automatically accept the connection
                    partnerEndpointName = endpointId;
                    Log.d(TAG, "onConnectionInit from" + endpointId);
                    mConnectionsClient.acceptConnection(endpointId, mPayloadCallback);

                }

                @Override
                public void onConnectionResult(String endpointId, ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode()) {
                        case ConnectionsStatusCodes.STATUS_OK:
                            //Finished establishing a correct connection
                            Log.d(TAG, "We're sucessfully Nearby connected!");
                            Toast.makeText(getApplicationContext(), "nearby connected (real time receiver)", Toast.LENGTH_SHORT).show();
                            nearbyConnected = true;
                            reattachReceivingNotification();
                            break;
                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            Log.d(TAG, "The connection was rejected by one or both sides");
                            break;
                        default:
                            Log.wtf(TAG, "The connection was broken before it was accepted.");
                            Toast.makeText(getApplicationContext(), "Wtf Connection Broke", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
                @Override
                public void onDisconnected(String endpointId) {
                    //mIsConnected = false;
                    Log.d(TAG, "We're disconnected");
                    Toast.makeText(getApplicationContext(), "nearby disconnected (real time receiver)", Toast.LENGTH_SHORT).show();
                    launchDiscovering();
                    nearbyConnected = false;
                    reattachReceivingNotification();
                }
            };
    //Nearby Payload Callback
    private final PayloadCallback mPayloadCallback =
            new PayloadCallback() {
                @Override
                public void onPayloadReceived(String endpointId, Payload payload) {
                    //refreshAmountTextView();
                    savePayload(payload);

                }
                @Override
                public void onPayloadTransferUpdate(String endpointId, PayloadTransferUpdate update) {
                    if (update.getStatus() == PayloadTransferUpdate.Status.SUCCESS) {
                        //Nothing do to here
                    }
                }
            };










    @Override
    public void onCreate() {
        super.onCreate();

        mConnectionsClient = Nearby.getConnectionsClient(this);
        //Service ID
        serviceId = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext()).getString(Constants.SETTINGS.KEY_PREF_TRANSFERID, "myUniqueID");

        realTimeAccelerationSaver  = new RealTimeAccelerationSaver();
        realTimeLinearAccelerationSaver = new RealTimeLinearAccelerationSaver();
        realTimeBarometerSaver = new RealTimeBarometerSaver();
        realTimeGPSSaver = new RealTimeGPSSaver();
        realTimeGyroscopeSaver = new RealTimeGyroscopeSaver();
        realTimeCompassSaver = new RealTimeCompassSaver();

        localBroadcastManager = LocalBroadcastManager.getInstance(this.getApplicationContext());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {

            // Start the Discovery
            if (intent.getAction().equals(Constants.ACTION.REALTIME_RECEIVER_ACTION_START)) {
                Log.i(TAG, "Received Realtime Start Service intent");

                if(!nearbyAdvertising){
                reattachReceivingNotification();
                launchDiscovering();

                Toast.makeText(this,"real time service started",Toast.LENGTH_SHORT).show();
                    nearbyAdvertising = true;
                } else{
                    Toast.makeText(this, "real time service already running!", Toast.LENGTH_SHORT).show();
                }
            }
            //Stop the Discovery
            if (intent.getAction().equals(Constants.ACTION.REALTIME_RECEIVER_ACTION_STOP)) {
                Log.i(TAG, "Received Realtime Stop Service intent");

                if(nearbyAdvertising) {
                    mConnectionsClient.stopDiscovery();
                    mConnectionsClient.stopAllEndpoints();

                    Toast.makeText(this, "real time service stopped", Toast.LENGTH_SHORT).show();
                    nearbyAdvertising = false;

                    stopForeground(true);
                    stopSelf();
                } else{
                    Toast.makeText(this, "can not stop real time service. not running!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        return START_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.i(TAG, "onDestroy of RealTimeTransferReceiverService is called");
    }


    public void reattachReceivingNotification() {
        stopForeground(true);

        // for best practice we have to set notificationChannel api > 25
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel notificationChannel = new NotificationChannel(Constants.ID.REAL_TIME_TRANSFER_RECEIVER_NOTIFICATION_CHANNEL_ID, Constants.ID.REAL_TIME_TRANSFER_RECEIVER_NOTIFICATION_CHANNEL_ID, NotificationManager.IMPORTANCE_HIGH);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        //notification intent
        Intent notificationIntent = new Intent(this, RealTimeTransferReceiverActivity.class);
        //notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);


        //stop receiving button intent
        Intent stopReceivingIntent = new Intent(this, RealTimeTransferReceiverService.class);
        stopReceivingIntent.setAction(Constants.ACTION.REALTIME_RECEIVER_ACTION_STOP);
        PendingIntent pstopReceivingIntent = PendingIntent.getService(this, 0, stopReceivingIntent, 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        NotificationCompat.Action stopAction = new NotificationCompat.Action.Builder(android.R.drawable.ic_lock_power_off, "Stop receiving", pstopReceivingIntent).build();

        if (nearbyConnected) {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, Constants.ID.REAL_TIME_TRANSFER_RECEIVER_NOTIFICATION_CHANNEL_ID)
                    .setContentTitle("Android Blackbox")
                    .setTicker("Receiving started!")
                    .setContentText("receiving...")
                    .setSmallIcon(android.R.drawable.stat_sys_download) //ic_popup_sync
                    .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                    .setContentIntent(pendingIntent)
                    .setOngoing(true)
                    .addAction(stopAction);
            Notification notification = builder.build();
            startForeground(Constants.ID.REAL_TIME_TRANSFER_RECEIVER_ID, notification);
        }
        else{
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, Constants.ID.REAL_TIME_TRANSFER_RECEIVER_NOTIFICATION_CHANNEL_ID)
                    .setContentTitle("Android Blackbox Receiver")
                    .setTicker("Looking for Blackbox Sender!")
                    .setContentText("awaiting connection")
                    .setSmallIcon(android.R.drawable.ic_popup_sync) //ic_popup_sync
                    .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                    .setContentIntent(pendingIntent)
                    .setOngoing(true)
                    .addAction(stopAction);
            Notification notification = builder.build();
            startForeground(Constants.ID.REAL_TIME_TRANSFER_RECEIVER_ID, notification);
        }
    }

    /*
   This Method takes a payload, analyzes it and give it to the corresponing Saver
    */
    private void savePayload(Payload payload) {
        String payloadString = "";
        try {
            payloadString = new String(payload.asBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // The 4 first letters in the PayloadString defines the type (example acc; or gyr;
        String typeString = payloadString.substring(0,4);
        String saveString = payloadString.substring(4);
        Log.v(TAG, "Received Data: "+ typeString);
        switch (typeString){
            case "acc;":
                realTimeAccelerationSaver.save(saveString);
                break;
            case "lac;":
                realTimeLinearAccelerationSaver.save(saveString);
                break;
            case "bar;":
                realTimeBarometerSaver.save(saveString);
                break;
            case "gyr;":
                realTimeGyroscopeSaver.save(saveString);
                break;
            case "com;":
                realTimeCompassSaver.save(saveString);
                break;
            case "gps;":
                realTimeGPSSaver.save(saveString);
                break;
            default:
                Log.wtf(TAG, "Unknow type... HOW can this happen?");

        }
        this.amountPayloads++;
        //Send an Intent to update the Amount GUI in the Activity:
        Intent localIntent = new Intent(Constants.ACTION.REALTIME_RECEIVER_GUI_UPDATE);
        localIntent.putExtra("amount", String.valueOf(this.amountPayloads));
        localBroadcastManager.sendBroadcast(localIntent);
    }


/*
Starts the Discovering for another Nearby Devie
 */
    private void launchDiscovering() {
        Log.d(TAG, "Starting Discovering for ID:" + serviceId);

        Toast.makeText(getApplicationContext(), "Nearby discovering for ID:" + serviceId, Toast.LENGTH_SHORT).show();
        mConnectionsClient
                .startDiscovery(
                        serviceId,
                        new EndpointDiscoveryCallback() {
                            @Override
                            public void onEndpointFound(String endpointId, DiscoveredEndpointInfo info) {
                                Log.d(TAG, "Endpoint Found, Name is: "+ endpointId);
                                if (serviceId.equals(info.getServiceId())) {
                                    partnerEndpointName = endpointId;

                                    // Ask for a Connection
                                    mConnectionsClient
                                            .requestConnection("DiscovererName", partnerEndpointName, mConnectionLifecycleCallback)
                                            .addOnFailureListener(
                                                    new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            Log.d(TAG, "onEndpointDiscovered failed" +e.toString());
                                                        }
                                                    });
                                }
                            }
                            @Override
                            public void onEndpointLost(String endpointId) {
                                Log.d(TAG, "onEndpointLost failed");
                            }
                        },
                        new DiscoveryOptions(strategy))
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unusedResult) {

                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "Discovery failed" +e.toString());
                            }
                        });
    }

}