# Android Blackbox

This software is a product of a bachelor thesis at the Bern University of Applied Sciences.

The Application (directory "MainPrototype") is a blackbox for android. It is possible to store various sensor data over a user-defined loop length and send the data over nearby to a secondary android device. The system has a crash recognition, if no crash is detected, the data will be purged.
For more information, see the “Benutzerhandbuch” (only in German) in the documents directory 
